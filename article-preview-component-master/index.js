let share = document.getElementById('share-icon');
let shareIcon = document.getElementById('share-icon-img');
let shareContainer = document.getElementById('share-container');

share.addEventListener('mouseenter', function(e) {
    shareIcon.style.filter = "brightness(3)";
    shareContainer.classList.toggle('hidden');
    e.preventDefault();
})

share.addEventListener('mouseleave', function(e) {
    shareIcon.style.filter = "brightness(1)";
    e.preventDefault();
})

shareContainer.addEventListener('mouseleave', function(e) {
    shareContainer.classList.toggle('hidden');
    e.preventDefault();
})