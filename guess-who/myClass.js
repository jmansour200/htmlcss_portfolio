class Player {
    constructor (first_name, last_name, team, role, games, height, age, number) {
        this.first_name = first_name; //done
        this.last_name = last_name; //done
        this.team = team; //done
        this.role = role; //done
        this.games = games; //done
        this.height = height; //done
        this.age = age; //done
        this.number = number; //done
    }
}

const teamColours = {     
    "ADE":["blue","gold","red"],
    "BRI":["blue","gold","red"],
    "CAR":["blue","white"],
    "COL":["black","white"],
    "ESS":["black","red"],
    "FRE":["purple","white"],
    "GEE":["blue","white"],
    "GCS":["gold","red"],
    "GWS":["grey","orange","white"],
    "HAW":["brown","gold"],
    "MEL":["blue","red"],
    "NOR":["blue","white"],
    "POR":["black","blue","white"],
    "RIC":["black","gold"],
    "STK":["black","red","white"],
    "SYD":["red","white"],
    "WBD":["blue","red","white"],
    "WCE":["blue","gold"]
};

const player_list = {
    "aaron cadman": {
       "first_name": "Aaron",
       "last_name": "Cadman",
       "current_team": "GWS",
       "number": 5,
       "games": 19,
       "age": 20,
       "height": 195,
       "role": "FWD"
    },
    "aaron francis": {
       "first_name": "Aaron",
       "last_name": "Francis",
       "current_team": "SYD",
       "number": 10,
       "games": 69,
       "age": 26,
       "height": 192,
       "role": "DEF"
    },
    "aaron naughton": {
       "first_name": "Aaron",
       "last_name": "Naughton",
       "current_team": "WBD",
       "number": 33,
       "games": 129,
       "age": 24,
       "height": 196,
       "role": "FWD"
    },
    "adam cerra": {
       "first_name": "Adam",
       "last_name": "Cerra",
       "current_team": "CAR",
       "number": 5,
       "games": 120,
       "age": 24,
       "height": 188,
       "role": "MID"
    },
    "adam kennedy": {
       "first_name": "Adam",
       "last_name": "Kennedy",
       "current_team": "GWS",
       "number": 40,
       "games": 153,
       "age": 31,
       "height": 183,
       "role": "DEF FWD"
    },
    "adam saad": {
       "first_name": "Adam",
       "last_name": "Saad",
       "current_team": "CAR",
       "number": 42,
       "games": 182,
       "age": 29,
       "height": 178,
       "role": "DEF"
    },
    "adam tomlinson": {
       "first_name": "Adam",
       "last_name": "Tomlinson",
       "current_team": "MEL",
       "number": 20,
       "games": 178,
       "age": 30,
       "height": 194,
       "role": "DEF"
    },
    "adam treloar": {
       "first_name": "Adam",
       "last_name": "Treloar",
       "current_team": "WBD",
       "number": 1,
       "games": 237,
       "age": 31,
       "height": 185,
       "role": "MID"
    },
    "aidan corr": {
       "first_name": "Aidan",
       "last_name": "Corr",
       "current_team": "NOR",
       "number": 4,
       "games": 145,
       "age": 29,
       "height": 195,
       "role": "DEF"
    },
    "aiden begg": {
       "first_name": "Aiden",
       "last_name": "Begg",
       "current_team": "COL",
       "number": 39,
       "games": 3,
       "age": 21,
       "height": 197,
       "role": "RCK"
    },
    "alex cincotta": {
       "first_name": "Alex",
       "last_name": "Cincotta",
       "current_team": "CAR",
       "number": 39,
       "games": 19,
       "age": 27,
       "height": 187,
       "role": "DEF"
    },
    "alex davies": {
       "first_name": "Alex",
       "last_name": "Davies",
       "current_team": "GCS",
       "number": 5,
       "games": 25,
       "age": 22,
       "height": 193,
       "role": "MID FWD"
    },
    "alex keath": {
       "first_name": "Alex",
       "last_name": "Keath",
       "current_team": "WBD",
       "number": 42,
       "games": 104,
       "age": 32,
       "height": 198,
       "role": "DEF"
    },
    "alex neal-bullen": {
       "first_name": "Alex",
       "last_name": "Neal-Bullen",
       "current_team": "MEL",
       "number": 30,
       "games": 160,
       "age": 28,
       "height": 182,
       "role": "FWD"
    },
    "alex pearce": {
       "first_name": "Alex",
       "last_name": "Pearce",
       "current_team": "FRE",
       "number": 25,
       "games": 113,
       "age": 28,
       "height": 201,
       "role": "DEF"
    },
    "alex sexton": {
       "first_name": "Alex",
       "last_name": "Sexton",
       "current_team": "GCS",
       "number": 6,
       "games": 169,
       "age": 30,
       "height": 186,
       "role": "DEF FWD"
    },
    "alex witherden": {
       "first_name": "Alex",
       "last_name": "Witherden",
       "current_team": "WCE",
       "number": 23,
       "games": 106,
       "age": 25,
       "height": 188,
       "role": "DEF"
    },
    "aliir aliir": {
       "first_name": "Aliir",
       "last_name": "Aliir",
       "current_team": "POR",
       "number": 21,
       "games": 137,
       "age": 29,
       "height": 194,
       "role": "DEF"
    },
    "alwyn davey": {
       "first_name": "Alwyn",
       "last_name": "Davey",
       "current_team": "ESS",
       "number": 33,
       "games": 14,
       "age": 20,
       "height": 181,
       "role": "FWD"
    },
    "andrew brayshaw": {
       "first_name": "Andrew",
       "last_name": "Brayshaw",
       "current_team": "FRE",
       "number": 8,
       "games": 129,
       "age": 24,
       "height": 185,
       "role": "MID"
    },
    "andrew gaff": {
       "first_name": "Andrew",
       "last_name": "Gaff",
       "current_team": "WCE",
       "number": 3,
       "games": 276,
       "age": 31,
       "height": 184,
       "role": "MID"
    },
    "andrew mcgrath": {
       "first_name": "Andrew",
       "last_name": "McGrath",
       "current_team": "ESS",
       "number": 1,
       "games": 141,
       "age": 25,
       "height": 180,
       "role": "DEF"
    },
    "angus brayshaw": {
       "first_name": "Angus",
       "last_name": "Brayshaw",
       "current_team": "MEL",
       "number": 10,
       "games": 167,
       "age": 28,
       "height": 188,
       "role": "DEF MID"
    },
    "angus hastie": {
       "first_name": "Angus",
       "last_name": "Hastie",
       "current_team": "STK",
       "number": 24,
       "games": 2,
       "age": 18,
       "height": 190,
       "role": "DEF"
    },
    "angus sheldrick": {
       "first_name": "Angus",
       "last_name": "Sheldrick",
       "current_team": "SYD",
       "number": 12,
       "games": 9,
       "age": 20,
       "height": 178,
       "role": "MID"
    },
    "anthony caminiti": {
       "first_name": "Anthony",
       "last_name": "Caminiti",
       "current_team": "STK",
       "number": 47,
       "games": 22,
       "age": 20,
       "height": 196,
       "role": "FWD"
    },
    "anthony scott": {
       "first_name": "Anthony",
       "last_name": "Scott",
       "current_team": "WBD",
       "number": 28,
       "games": 57,
       "age": 29,
       "height": 181,
       "role": "MID FWD"
    },
    "archie perkins": {
       "first_name": "Archie",
       "last_name": "Perkins",
       "current_team": "ESS",
       "number": 16,
       "games": 66,
       "age": 22,
       "height": 188,
       "role": "MID FWD"
    },
    "arthur jones": {
       "first_name": "Arthur",
       "last_name": "Jones",
       "current_team": "WBD",
       "number": 32,
       "games": 13,
       "age": 20,
       "height": 180,
       "role": "FWD"
    },
    "ash johnson": {
       "first_name": "Ash",
       "last_name": "Johnson",
       "current_team": "COL",
       "number": 40,
       "games": 26,
       "age": 26,
       "height": 193,
       "role": "FWD"
    },
    "bailey banfield": {
       "first_name": "Bailey",
       "last_name": "Banfield",
       "current_team": "FRE",
       "number": 41,
       "games": 81,
       "age": 26,
       "height": 190,
       "role": "FWD"
    },
    "bailey dale": {
       "first_name": "Bailey",
       "last_name": "Dale",
       "current_team": "WBD",
       "number": 31,
       "games": 137,
       "age": 27,
       "height": 188,
       "role": "DEF"
    },
    "bailey humphrey": {
       "first_name": "Bailey",
       "last_name": "Humphrey",
       "current_team": "GCS",
       "number": 19,
       "games": 24,
       "age": 19,
       "height": 186,
       "role": "FWD"
    },
    "bailey j. williams": {
       "first_name": "Bailey J.",
       "last_name": "Williams",
       "current_team": "WCE",
       "number": 32,
       "games": 55,
       "age": 24,
       "height": 201,
       "role": "RCK"
    },
    "bailey laurie": {
       "first_name": "Bailey",
       "last_name": "Laurie",
       "current_team": "MEL",
       "number": 16,
       "games": 7,
       "age": 22,
       "height": 179,
       "role": "FWD"
    },
    "bailey macdonald": {
       "first_name": "Bailey",
       "last_name": "Macdonald",
       "current_team": "HAW",
       "number": 42,
       "games": 2,
       "age": 19,
       "height": 182,
       "role": "DEF"
    },
    "bailey scott": {
       "first_name": "Bailey",
       "last_name": "Scott",
       "current_team": "NOR",
       "number": 8,
       "games": 84,
       "age": 23,
       "height": 186,
       "role": "DEF MID"
    },
    "bailey smith": {
       "first_name": "Bailey",
       "last_name": "Smith",
       "current_team": "WBD",
       "number": 6,
       "games": 103,
       "age": 23,
       "height": 185,
       "role": "MID FWD"
    },
    "bailey williams": {
       "first_name": "Bailey",
       "last_name": "Williams",
       "current_team": "WBD",
       "number": 34,
       "games": 133,
       "age": 26,
       "height": 190,
       "role": "MID"
    },
    "bayley fritsch": {
       "first_name": "Bayley",
       "last_name": "Fritsch",
       "current_team": "MEL",
       "number": 31,
       "games": 133,
       "age": 27,
       "height": 188,
       "role": "FWD"
    },
    "beau mccreery": {
       "first_name": "Beau",
       "last_name": "McCreery",
       "current_team": "COL",
       "number": 31,
       "games": 67,
       "age": 23,
       "height": 186,
       "role": "FWD"
    },
    "ben ainsworth": {
       "first_name": "Ben",
       "last_name": "Ainsworth",
       "current_team": "GCS",
       "number": 9,
       "games": 123,
       "age": 26,
       "height": 178,
       "role": "FWD"
    },
    "ben brown": {
       "first_name": "Ben",
       "last_name": "Brown",
       "current_team": "MEL",
       "number": 50,
       "games": 174,
       "age": 31,
       "height": 200,
       "role": "FWD"
    },
    "ben hobbs": {
       "first_name": "Ben",
       "last_name": "Hobbs",
       "current_team": "ESS",
       "number": 8,
       "games": 41,
       "age": 20,
       "height": 183,
       "role": "MID FWD"
    },
    "ben keays": {
       "first_name": "Ben",
       "last_name": "Keays",
       "current_team": "ADE",
       "number": 2,
       "games": 119,
       "age": 27,
       "height": 186,
       "role": "FWD"
    },
    "ben king": {
       "first_name": "Ben",
       "last_name": "King",
       "current_team": "GCS",
       "number": 34,
       "games": 79,
       "age": 23,
       "height": 202,
       "role": "FWD"
    },
    "ben long": {
       "first_name": "Ben",
       "last_name": "Long",
       "current_team": "GCS",
       "number": 22,
       "games": 95,
       "age": 26,
       "height": 186,
       "role": "DEF"
    },
    "ben mckay": {
       "first_name": "Ben",
       "last_name": "McKay",
       "current_team": "ESS",
       "number": 32,
       "games": 78,
       "age": 26,
       "height": 202,
       "role": "DEF"
    },
    "ben miller": {
       "first_name": "Ben",
       "last_name": "Miller",
       "current_team": "RIC",
       "number": 46,
       "games": 30,
       "age": 24,
       "height": 198,
       "role": "FWD RCK"
    },
    "ben paton": {
       "first_name": "Ben",
       "last_name": "Paton",
       "current_team": "STK",
       "number": 33,
       "games": 67,
       "age": 25,
       "height": 187,
       "role": "DEF"
    },
    "bigoa nyuon": {
       "first_name": "Bigoa",
       "last_name": "Nyuon",
       "current_team": "NOR",
       "number": 28,
       "games": 3,
       "age": 22,
       "height": 197,
       "role": "FWD"
    },
    "billy frampton": {
       "first_name": "Billy",
       "last_name": "Frampton",
       "current_team": "COL",
       "number": 17,
       "games": 45,
       "age": 27,
       "height": 201,
       "role": "DEF"
    },
    "blake acres": {
       "first_name": "Blake",
       "last_name": "Acres",
       "current_team": "CAR",
       "number": 13,
       "games": 151,
       "age": 28,
       "height": 193,
       "role": "MID"
    },
    "blake drury": {
       "first_name": "Blake",
       "last_name": "Drury",
       "current_team": "NOR",
       "number": 41,
       "games": 6,
       "age": 20,
       "height": 177,
       "role": "FWD"
    },
    "blake hardwick": {
       "first_name": "Blake",
       "last_name": "Hardwick",
       "current_team": "HAW",
       "number": 15,
       "games": 153,
       "age": 27,
       "height": 183,
       "role": "DEF FWD"
    },
    "blake howes": {
       "first_name": "Blake",
       "last_name": "Howes",
       "current_team": "MEL",
       "number": 22,
       "games": 7,
       "age": 21,
       "height": 191,
       "role": "DEF"
    },
    "bobby hill": {
       "first_name": "Bobby",
       "last_name": "Hill",
       "current_team": "COL",
       "number": 23,
       "games": 72,
       "age": 24,
       "height": 175,
       "role": "FWD"
    },
    "bodhi uwland": {
       "first_name": "Bodhi",
       "last_name": "Uwland",
       "current_team": "GCS",
       "number": 32,
       "games": 9,
       "age": 20,
       "height": 188,
       "role": "DEF"
    },
    "brad crouch": {
       "first_name": "Brad",
       "last_name": "Crouch",
       "current_team": "STK",
       "number": 5,
       "games": 161,
       "age": 30,
       "height": 186,
       "role": "MID"
    },
    "bradley close": {
       "first_name": "Bradley",
       "last_name": "Close",
       "current_team": "GEE",
       "number": 45,
       "games": 82,
       "age": 25,
       "height": 183,
       "role": "FWD"
    },
    "bradley hill": {
       "first_name": "Bradley",
       "last_name": "Hill",
       "current_team": "STK",
       "number": 8,
       "games": 239,
       "age": 30,
       "height": 182,
       "role": "MID FWD"
    },
    "brady hough": {
       "first_name": "Brady",
       "last_name": "Hough",
       "current_team": "WCE",
       "number": 19,
       "games": 36,
       "age": 21,
       "height": 191,
       "role": "DEF"
    },
    "braeden campbell": {
       "first_name": "Braeden",
       "last_name": "Campbell",
       "current_team": "SYD",
       "number": 16,
       "games": 54,
       "age": 22,
       "height": 181,
       "role": "DEF MID"
    },
    "brandan parfitt": {
       "first_name": "Brandan",
       "last_name": "Parfitt",
       "current_team": "GEE",
       "number": 3,
       "games": 126,
       "age": 25,
       "height": 180,
       "role": "MID"
    },
    "brandon ellis": {
       "first_name": "Brandon",
       "last_name": "Ellis",
       "current_team": "GCS",
       "number": 4,
       "games": 250,
       "age": 30,
       "height": 181,
       "role": "MID"
    },
    "brandon ryan": {
       "first_name": "Brandon",
       "last_name": "Ryan",
       "current_team": "BRI",
       "number": 24,
       "games": 3,
       "age": 26,
       "height": 200,
       "role": "FWD"
    },
    "brandon starcevich": {
       "first_name": "Brandon",
       "last_name": "Starcevich",
       "current_team": "BRI",
       "number": 37,
       "games": 104,
       "age": 24,
       "height": 187,
       "role": "DEF"
    },
    "brandon walker": {
       "first_name": "Brandon",
       "last_name": "Walker",
       "current_team": "FRE",
       "number": 31,
       "games": 50,
       "age": 21,
       "height": 186,
       "role": "DEF"
    },
    "brandon zerk-thatcher": {
       "first_name": "Brandon",
       "last_name": "Zerk-Thatcher",
       "current_team": "POR",
       "number": 25,
       "games": 57,
       "age": 25,
       "height": 195,
       "role": "DEF"
    },
    "brayden cook": {
       "first_name": "Brayden",
       "last_name": "Cook",
       "current_team": "ADE",
       "number": 15,
       "games": 15,
       "age": 21,
       "height": 191,
       "role": "MID FWD"
    },
    "brayden fiorini": {
       "first_name": "Brayden",
       "last_name": "Fiorini",
       "current_team": "GCS",
       "number": 8,
       "games": 99,
       "age": 26,
       "height": 187,
       "role": "MID"
    },
    "brayden maynard": {
       "first_name": "Brayden",
       "last_name": "Maynard",
       "current_team": "COL",
       "number": 4,
       "games": 193,
       "age": 27,
       "height": 189,
       "role": "DEF"
    },
    "braydon preuss": {
       "first_name": "Braydon",
       "last_name": "Preuss",
       "current_team": "GWS",
       "number": 11,
       "games": 28,
       "age": 28,
       "height": 207,
       "role": "RCK"
    },
    "brennan cox": {
       "first_name": "Brennan",
       "last_name": "Cox",
       "current_team": "FRE",
       "number": 36,
       "games": 103,
       "age": 25,
       "height": 195,
       "role": "DEF"
    },
    "brent daniels": {
       "first_name": "Brent",
       "last_name": "Daniels",
       "current_team": "GWS",
       "number": 16,
       "games": 88,
       "age": 25,
       "height": 172,
       "role": "FWD"
    },
    "brodie grundy": {
       "first_name": "Brodie",
       "last_name": "Grundy",
       "current_team": "SYD",
       "number": 4,
       "games": 200,
       "age": 30,
       "height": 202,
       "role": "RCK"
    },
    "brodie kemp": {
       "first_name": "Brodie",
       "last_name": "Kemp",
       "current_team": "CAR",
       "number": 17,
       "games": 28,
       "age": 22,
       "height": 192,
       "role": "DEF"
    },
    "brodie smith": {
       "first_name": "Brodie",
       "last_name": "Smith",
       "current_team": "ADE",
       "number": 33,
       "games": 253,
       "age": 32,
       "height": 189,
       "role": "DEF"
    },
    "brody mihocek": {
       "first_name": "Brody",
       "last_name": "Mihocek",
       "current_team": "COL",
       "number": 41,
       "games": 133,
       "age": 31,
       "height": 192,
       "role": "FWD"
    },
    "buku khamis": {
       "first_name": "Buku",
       "last_name": "Khamis",
       "current_team": "WBD",
       "number": 24,
       "games": 16,
       "age": 24,
       "height": 191,
       "role": "DEF FWD"
    },
    "caleb daniel": {
       "first_name": "Caleb",
       "last_name": "Daniel",
       "current_team": "WBD",
       "number": 35,
       "games": 180,
       "age": 27,
       "height": 171,
       "role": "MID FWD"
    },
    "caleb graham": {
       "first_name": "Caleb",
       "last_name": "Graham",
       "current_team": "GCS",
       "number": 46,
       "games": 37,
       "age": 23,
       "height": 194,
       "role": "DEF"
    },
    "caleb marchbank": {
       "first_name": "Caleb",
       "last_name": "Marchbank",
       "current_team": "CAR",
       "number": 22,
       "games": 61,
       "age": 27,
       "height": 193,
       "role": "DEF"
    },
    "caleb mitchell": {
       "first_name": "Caleb",
       "last_name": "Mitchell",
       "current_team": "SYD",
       "number": 35,
       "games": 2,
       "age": 19,
       "height": 186,
       "role": "DEF MID"
    },
    "caleb poulter": {
       "first_name": "Caleb",
       "last_name": "Poulter",
       "current_team": "WBD",
       "number": 25,
       "games": 23,
       "age": 21,
       "height": 194,
       "role": "MID"
    },
    "caleb serong": {
       "first_name": "Caleb",
       "last_name": "Serong",
       "current_team": "FRE",
       "number": 3,
       "games": 86,
       "age": 23,
       "height": 180,
       "role": "MID"
    },
    "caleb windsor": {
       "first_name": "Caleb",
       "last_name": "Windsor",
       "current_team": "MEL",
       "number": 6,
       "games": 7,
       "age": 18,
       "height": 185,
       "role": "MID FWD"
    },
    "callan ward": {
       "first_name": "Callan",
       "last_name": "Ward",
       "current_team": "GWS",
       "number": 8,
       "games": 298,
       "age": 34,
       "height": 187,
       "role": "MID"
    },
    "callum ah chee": {
       "first_name": "Callum",
       "last_name": "Ah Chee",
       "current_team": "BRI",
       "number": 4,
       "games": 124,
       "age": 26,
       "height": 183,
       "role": "MID"
    },
    "callum brown": {
       "first_name": "Callum",
       "last_name": "Brown",
       "current_team": "GWS",
       "number": 46,
       "games": 36,
       "age": 23,
       "height": 188,
       "role": "FWD"
    },
    "callum coleman-jones": {
       "first_name": "Callum",
       "last_name": "Coleman-Jones",
       "current_team": "NOR",
       "number": 21,
       "games": 31,
       "age": 24,
       "height": 200,
       "role": "FWD"
    },
    "callum jamieson": {
       "first_name": "Callum",
       "last_name": "Jamieson",
       "current_team": "WCE",
       "number": 40,
       "games": 17,
       "age": 23,
       "height": 201,
       "role": "DEF FWD"
    },
    "callum mills": {
       "first_name": "Callum",
       "last_name": "Mills",
       "current_team": "SYD",
       "number": 14,
       "games": 155,
       "age": 27,
       "height": 188,
       "role": "MID"
    },
    "callum wilkie": {
       "first_name": "Callum",
       "last_name": "Wilkie",
       "current_team": "STK",
       "number": 44,
       "games": 115,
       "age": 28,
       "height": 191,
       "role": "DEF"
    },
    "cameron guthrie": {
       "first_name": "Cameron",
       "last_name": "Guthrie",
       "current_team": "GEE",
       "number": 29,
       "games": 236,
       "age": 31,
       "height": 187,
       "role": "MID"
    },
    "cameron mackenzie": {
       "first_name": "Cameron",
       "last_name": "Mackenzie",
       "current_team": "HAW",
       "number": 28,
       "games": 20,
       "age": 20,
       "height": 188,
       "role": "MID FWD"
    },
    "cameron rayner": {
       "first_name": "Cameron",
       "last_name": "Rayner",
       "current_team": "BRI",
       "number": 16,
       "games": 120,
       "age": 24,
       "height": 186,
       "role": "MID FWD"
    },
    "cameron zurhaar": {
       "first_name": "Cameron",
       "last_name": "Zurhaar",
       "current_team": "NOR",
       "number": 44,
       "games": 104,
       "age": 25,
       "height": 190,
       "role": "FWD"
    },
    "campbell chesser": {
       "first_name": "Campbell",
       "last_name": "Chesser",
       "current_team": "WCE",
       "number": 18,
       "games": 20,
       "age": 20,
       "height": 186,
       "role": "MID"
    },
    "carter michael": {
       "first_name": "Carter",
       "last_name": "Michael",
       "current_team": "BRI",
       "number": 39,
       "games": 1,
       "age": 21,
       "height": 188,
       "role": "DEF"
    },
    "chad warner": {
       "first_name": "Chad",
       "last_name": "Warner",
       "current_team": "SYD",
       "number": 1,
       "games": 66,
       "age": 22,
       "height": 185,
       "role": "MID"
    },
    "chad wingard": {
       "first_name": "Chad",
       "last_name": "Wingard",
       "current_team": "HAW",
       "number": 20,
       "games": 218,
       "age": 30,
       "height": 183,
       "role": "FWD"
    },
    "changkuoth jiath": {
       "first_name": "Changkuoth",
       "last_name": "Jiath",
       "current_team": "HAW",
       "number": 9,
       "games": 45,
       "age": 24,
       "height": 185,
       "role": "DEF"
    },
    "charlie ballard": {
       "first_name": "Charlie",
       "last_name": "Ballard",
       "current_team": "GCS",
       "number": 10,
       "games": 116,
       "age": 24,
       "height": 196,
       "role": "DEF"
    },
    "charlie cameron": {
       "first_name": "Charlie",
       "last_name": "Cameron",
       "current_team": "BRI",
       "number": 23,
       "games": 209,
       "age": 29,
       "height": 180,
       "role": "FWD"
    },
    "charlie comben": {
       "first_name": "Charlie",
       "last_name": "Comben",
       "current_team": "NOR",
       "number": 30,
       "games": 12,
       "age": 22,
       "height": 199,
       "role": "DEF FWD"
    },
    "charlie curnow": {
       "first_name": "Charlie",
       "last_name": "Curnow",
       "current_team": "CAR",
       "number": 30,
       "games": 116,
       "age": 27,
       "height": 194,
       "role": "FWD"
    },
    "charlie dean": {
       "first_name": "Charlie",
       "last_name": "Dean",
       "current_team": "COL",
       "number": 43,
       "games": 2,
       "age": 22,
       "height": 195,
       "role": "DEF"
    },
    "charlie dixon": {
       "first_name": "Charlie",
       "last_name": "Dixon",
       "current_team": "POR",
       "number": 22,
       "games": 208,
       "age": 33,
       "height": 200,
       "role": "FWD"
    },
    "charlie lazzaro": {
       "first_name": "Charlie",
       "last_name": "Lazzaro",
       "current_team": "NOR",
       "number": 35,
       "games": 35,
       "age": 22,
       "height": 180,
       "role": "MID FWD"
    },
    "charlie spargo": {
       "first_name": "Charlie",
       "last_name": "Spargo",
       "current_team": "MEL",
       "number": 9,
       "games": 98,
       "age": 24,
       "height": 172,
       "role": "FWD"
    },
    "chayce jones": {
       "first_name": "Chayce",
       "last_name": "Jones",
       "current_team": "ADE",
       "number": 1,
       "games": 82,
       "age": 24,
       "height": 182,
       "role": "DEF MID"
    },
    "chris burgess": {
       "first_name": "Chris",
       "last_name": "Burgess",
       "current_team": "ADE",
       "number": 21,
       "games": 40,
       "age": 28,
       "height": 194,
       "role": "FWD"
    },
    "christian petracca": {
       "first_name": "Christian",
       "last_name": "Petracca",
       "current_team": "MEL",
       "number": 5,
       "games": 183,
       "age": 28,
       "height": 187,
       "role": "MID"
    },
    "christian salem": {
       "first_name": "Christian",
       "last_name": "Salem",
       "current_team": "MEL",
       "number": 3,
       "games": 165,
       "age": 28,
       "height": 184,
       "role": "DEF MID"
    },
    "clayton oliver": {
       "first_name": "Clayton",
       "last_name": "Oliver",
       "current_team": "MEL",
       "number": 13,
       "games": 169,
       "age": 26,
       "height": 189,
       "role": "MID"
    },
    "cody weightman": {
       "first_name": "Cody",
       "last_name": "Weightman",
       "current_team": "WBD",
       "number": 3,
       "games": 65,
       "age": 23,
       "height": 179,
       "role": "FWD"
    },
    "colby mckercher": {
       "first_name": "Colby",
       "last_name": "McKercher",
       "current_team": "NOR",
       "number": 10,
       "games": 6,
       "age": 19,
       "height": 182,
       "role": "DEF MID"
    },
    "connor budarick": {
       "first_name": "Connor",
       "last_name": "Budarick",
       "current_team": "GCS",
       "number": 35,
       "games": 31,
       "age": 23,
       "height": 175,
       "role": "DEF"
    },
    "connor idun": {
       "first_name": "Connor",
       "last_name": "Idun",
       "current_team": "GWS",
       "number": 39,
       "games": 75,
       "age": 23,
       "height": 193,
       "role": "DEF"
    },
    "connor macdonald": {
       "first_name": "Connor",
       "last_name": "MacDonald",
       "current_team": "HAW",
       "number": 31,
       "games": 47,
       "age": 21,
       "height": 185,
       "role": "FWD"
    },
    "connor o'sullivan": {
       "first_name": "Connor",
       "last_name": "O'Sullivan",
       "current_team": "GEE",
       "number": 14,
       "games": 1,
       "age": 18,
       "height": 198,
       "role": "DEF"
    },
    "connor rozee": {
       "first_name": "Connor",
       "last_name": "Rozee",
       "current_team": "POR",
       "number": 1,
       "games": 112,
       "age": 24,
       "height": 185,
       "role": "MID"
    },
    "conor mckenna": {
       "first_name": "Conor",
       "last_name": "McKenna",
       "current_team": "BRI",
       "number": 26,
       "games": 107,
       "age": 28,
       "height": 184,
       "role": "DEF"
    },
    "conor nash": {
       "first_name": "Conor",
       "last_name": "Nash",
       "current_team": "HAW",
       "number": 11,
       "games": 79,
       "age": 25,
       "height": 198,
       "role": "MID"
    },
    "conor stone": {
       "first_name": "Conor",
       "last_name": "Stone",
       "current_team": "GWS",
       "number": 18,
       "games": 9,
       "age": 22,
       "height": 189,
       "role": "FWD"
    },
    "cooper hamilton": {
       "first_name": "Cooper",
       "last_name": "Hamilton",
       "current_team": "GWS",
       "number": 43,
       "games": 8,
       "age": 20,
       "height": 185,
       "role": "MID"
    },
    "cooper harvey": {
       "first_name": "Cooper",
       "last_name": "Harvey",
       "current_team": "NOR",
       "number": 37,
       "games": 3,
       "age": 19,
       "height": 180,
       "role": "FWD"
    },
    "cooper sharman": {
       "first_name": "Cooper",
       "last_name": "Sharman",
       "current_team": "STK",
       "number": 43,
       "games": 37,
       "age": 23,
       "height": 193,
       "role": "FWD"
    },
    "cooper stephens": {
       "first_name": "Cooper",
       "last_name": "Stephens",
       "current_team": "HAW",
       "number": 21,
       "games": 7,
       "age": 23,
       "height": 188,
       "role": "MID"
    },
    "corey durdin": {
       "first_name": "Corey",
       "last_name": "Durdin",
       "current_team": "CAR",
       "number": 19,
       "games": 39,
       "age": 22,
       "height": 173,
       "role": "FWD"
    },
    "corey wagner": {
       "first_name": "Corey",
       "last_name": "Wagner",
       "current_team": "FRE",
       "number": 34,
       "games": 28,
       "age": 27,
       "height": 181,
       "role": "DEF"
    },
    "corey warner": {
       "first_name": "Corey",
       "last_name": "Warner",
       "current_team": "SYD",
       "number": 37,
       "games": 5,
       "age": 20,
       "height": 182,
       "role": "FWD"
    },
    "curtis taylor": {
       "first_name": "Curtis",
       "last_name": "Taylor",
       "current_team": "NOR",
       "number": 5,
       "games": 68,
       "age": 24,
       "height": 188,
       "role": "MID FWD"
    },
    "dan houston": {
       "first_name": "Dan",
       "last_name": "Houston",
       "current_team": "POR",
       "number": 5,
       "games": 152,
       "age": 26,
       "height": 186,
       "role": "DEF"
    },
    "dane rampe": {
       "first_name": "Dane",
       "last_name": "Rampe",
       "current_team": "SYD",
       "number": 24,
       "games": 234,
       "age": 33,
       "height": 187,
       "role": "DEF"
    },
    "daniel butler": {
       "first_name": "Daniel",
       "last_name": "Butler",
       "current_team": "STK",
       "number": 16,
       "games": 133,
       "age": 27,
       "height": 182,
       "role": "FWD"
    },
    "daniel mcstay": {
       "first_name": "Daniel",
       "last_name": "McStay",
       "current_team": "COL",
       "number": 11,
       "games": 175,
       "age": 28,
       "height": 195,
       "role": "FWD"
    },
    "daniel rioli": {
       "first_name": "Daniel",
       "last_name": "Rioli",
       "current_team": "RIC",
       "number": 17,
       "games": 167,
       "age": 27,
       "height": 179,
       "role": "DEF"
    },
    "daniel turner": {
       "first_name": "Daniel",
       "last_name": "Turner",
       "current_team": "MEL",
       "number": 42,
       "games": 4,
       "age": 22,
       "height": 194,
       "role": "DEF"
    },
    "dante visentini": {
       "first_name": "Dante",
       "last_name": "Visentini",
       "current_team": "POR",
       "number": 38,
       "games": 3,
       "age": 21,
       "height": 202,
       "role": "RCK"
    },
    "darcy byrne-jones": {
       "first_name": "Darcy",
       "last_name": "Byrne-Jones",
       "current_team": "POR",
       "number": 33,
       "games": 179,
       "age": 28,
       "height": 181,
       "role": "FWD"
    },
    "darcy cameron": {
       "first_name": "Darcy",
       "last_name": "Cameron",
       "current_team": "COL",
       "number": 14,
       "games": 79,
       "age": 28,
       "height": 204,
       "role": "RCK"
    },
    "darcy fogarty": {
       "first_name": "Darcy",
       "last_name": "Fogarty",
       "current_team": "ADE",
       "number": 32,
       "games": 85,
       "age": 24,
       "height": 194,
       "role": "FWD"
    },
    "darcy fort": {
       "first_name": "Darcy",
       "last_name": "Fort",
       "current_team": "BRI",
       "number": 32,
       "games": 35,
       "age": 30,
       "height": 204,
       "role": "FWD RCK"
    },
    "darcy gardiner": {
       "first_name": "Darcy",
       "last_name": "Gardiner",
       "current_team": "BRI",
       "number": 27,
       "games": 164,
       "age": 28,
       "height": 192,
       "role": "DEF FWD"
    },
    "darcy jones": {
       "first_name": "Darcy",
       "last_name": "Jones",
       "current_team": "GWS",
       "number": 2,
       "games": 1,
       "age": 20,
       "height": 175,
       "role": "FWD"
    },
    "darcy macpherson": {
       "first_name": "Darcy",
       "last_name": "Macpherson",
       "current_team": "GCS",
       "number": 44,
       "games": 97,
       "age": 26,
       "height": 175,
       "role": "DEF"
    },
    "darcy moore": {
       "first_name": "Darcy",
       "last_name": "Moore",
       "current_team": "COL",
       "number": 30,
       "games": 157,
       "age": 28,
       "height": 203,
       "role": "DEF"
    },
    "darcy parish": {
       "first_name": "Darcy",
       "last_name": "Parish",
       "current_team": "ESS",
       "number": 3,
       "games": 155,
       "age": 26,
       "height": 180,
       "role": "MID"
    },
    "darcy tucker": {
       "first_name": "Darcy",
       "last_name": "Tucker",
       "current_team": "NOR",
       "number": 13,
       "games": 132,
       "age": 27,
       "height": 184,
       "role": "MID"
    },
    "darcy wilmot": {
       "first_name": "Darcy",
       "last_name": "Wilmot",
       "current_team": "BRI",
       "number": 44,
       "games": 36,
       "age": 20,
       "height": 183,
       "role": "DEF"
    },
    "darcy wilson": {
       "first_name": "Darcy",
       "last_name": "Wilson",
       "current_team": "STK",
       "number": 22,
       "games": 6,
       "age": 18,
       "height": 185,
       "role": "MID FWD"
    },
    "darragh joyce": {
       "first_name": "Darragh",
       "last_name": "Joyce",
       "current_team": "BRI",
       "number": 41,
       "games": 18,
       "age": 27,
       "height": 195,
       "role": "DEF"
    },
    "david cuningham": {
       "first_name": "David",
       "last_name": "Cuningham",
       "current_team": "CAR",
       "number": 28,
       "games": 55,
       "age": 27,
       "height": 183,
       "role": "FWD"
    },
    "david swallow": {
       "first_name": "David",
       "last_name": "Swallow",
       "current_team": "GCS",
       "number": 24,
       "games": 226,
       "age": 31,
       "height": 186,
       "role": "MID FWD"
    },
    "dayne zorko": {
       "first_name": "Dayne",
       "last_name": "Zorko",
       "current_team": "BRI",
       "number": 15,
       "games": 257,
       "age": 35,
       "height": 175,
       "role": "MID FWD"
    },
    "denver grainger-barras": {
       "first_name": "Denver",
       "last_name": "Grainger-Barras",
       "current_team": "HAW",
       "number": 24,
       "games": 28,
       "age": 22,
       "height": 194,
       "role": "DEF FWD"
    },
    "deven robertson": {
       "first_name": "Deven",
       "last_name": "Robertson",
       "current_team": "BRI",
       "number": 2,
       "games": 43,
       "age": 22,
       "height": 185,
       "role": "MID FWD"
    },
    "dion prestia": {
       "first_name": "Dion",
       "last_name": "Prestia",
       "current_team": "RIC",
       "number": 3,
       "games": 214,
       "age": 31,
       "height": 175,
       "role": "MID"
    },
    "dominic bedendo": {
       "first_name": "Dominic",
       "last_name": "Bedendo",
       "current_team": "WBD",
       "number": 26,
       "games": 2,
       "age": 21,
       "height": 191,
       "role": "MID"
    },
    "dominic sheed": {
       "first_name": "Dominic",
       "last_name": "Sheed",
       "current_team": "WCE",
       "number": 4,
       "games": 158,
       "age": 29,
       "height": 186,
       "role": "MID"
    },
    "dougal howard": {
       "first_name": "Dougal",
       "last_name": "Howard",
       "current_team": "STK",
       "number": 20,
       "games": 120,
       "age": 28,
       "height": 199,
       "role": "DEF"
    },
    "dustin martin": {
       "first_name": "Dustin",
       "last_name": "Martin",
       "current_team": "RIC",
       "number": 4,
       "games": 294,
       "age": 32,
       "height": 187,
       "role": "FWD"
    },
    "dylan grimes": {
       "first_name": "Dylan",
       "last_name": "Grimes",
       "current_team": "RIC",
       "number": 2,
       "games": 233,
       "age": 32,
       "height": 194,
       "role": "DEF"
    },
    "dylan moore": {
       "first_name": "Dylan",
       "last_name": "Moore",
       "current_team": "HAW",
       "number": 13,
       "games": 81,
       "age": 24,
       "height": 177,
       "role": "FWD"
    },
    "dylan shiel": {
       "first_name": "Dylan",
       "last_name": "Shiel",
       "current_team": "ESS",
       "number": 9,
       "games": 211,
       "age": 31,
       "height": 182,
       "role": "MID"
    },
    "dylan stephens": {
       "first_name": "Dylan",
       "last_name": "Stephens",
       "current_team": "NOR",
       "number": 15,
       "games": 49,
       "age": 23,
       "height": 184,
       "role": "MID"
    },
    "dylan williams": {
       "first_name": "Dylan",
       "last_name": "Williams",
       "current_team": "POR",
       "number": 23,
       "games": 23,
       "age": 22,
       "height": 186,
       "role": "DEF"
    },
    "dyson heppell": {
       "first_name": "Dyson",
       "last_name": "Heppell",
       "current_team": "ESS",
       "number": 21,
       "games": 242,
       "age": 31,
       "height": 189,
       "role": "DEF"
    },
    "ed langdon": {
       "first_name": "Ed",
       "last_name": "Langdon",
       "current_team": "MEL",
       "number": 15,
       "games": 164,
       "age": 28,
       "height": 182,
       "role": "MID"
    },
    "ed richards": {
       "first_name": "Ed",
       "last_name": "Richards",
       "current_team": "WBD",
       "number": 20,
       "games": 109,
       "age": 24,
       "height": 188,
       "role": "DEF"
    },
    "eddie ford": {
       "first_name": "Eddie",
       "last_name": "Ford",
       "current_team": "NOR",
       "number": 40,
       "games": 28,
       "age": 21,
       "height": 189,
       "role": "FWD"
    },
    "elijah hewett": {
       "first_name": "Elijah",
       "last_name": "Hewett",
       "current_team": "WCE",
       "number": 8,
       "games": 14,
       "age": 19,
       "height": 186,
       "role": "MID FWD"
    },
    "elijah hollands": {
       "first_name": "Elijah",
       "last_name": "Hollands",
       "current_team": "CAR",
       "number": 20,
       "games": 18,
       "age": 22,
       "height": 189,
       "role": "MID FWD"
    },
    "elijah tsatas": {
       "first_name": "Elijah",
       "last_name": "Tsatas",
       "current_team": "ESS",
       "number": 5,
       "games": 7,
       "age": 19,
       "height": 187,
       "role": "MID FWD"
    },
    "elliot yeo": {
       "first_name": "Elliot",
       "last_name": "Yeo",
       "current_team": "WCE",
       "number": 6,
       "games": 198,
       "age": 30,
       "height": 191,
       "role": "DEF MID"
    },
    "elliott himmelberg": {
       "first_name": "Elliott",
       "last_name": "Himmelberg",
       "current_team": "ADE",
       "number": 34,
       "games": 41,
       "age": 25,
       "height": 199,
       "role": "FWD"
    },
    "emerson jeka": {
       "first_name": "Emerson",
       "last_name": "Jeka",
       "current_team": "GEE",
       "number": 43,
       "games": 7,
       "age": 22,
       "height": 198,
       "role": "DEF"
    },
    "eric hipwood": {
       "first_name": "Eric",
       "last_name": "Hipwood",
       "current_team": "BRI",
       "number": 30,
       "games": 160,
       "age": 26,
       "height": 203,
       "role": "FWD"
    },
    "errol gulden": {
       "first_name": "Errol",
       "last_name": "Gulden",
       "current_team": "SYD",
       "number": 21,
       "games": 73,
       "age": 21,
       "height": 175,
       "role": "MID"
    },
    "esava ratugolea": {
       "first_name": "Esava",
       "last_name": "Ratugolea",
       "current_team": "POR",
       "number": 27,
       "games": 81,
       "age": 25,
       "height": 197,
       "role": "DEF"
    },
    "ethan hughes": {
       "first_name": "Ethan",
       "last_name": "Hughes",
       "current_team": "FRE",
       "number": 15,
       "games": 107,
       "age": 29,
       "height": 188,
       "role": "DEF"
    },
    "ethan read": {
       "first_name": "Ethan",
       "last_name": "Read",
       "current_team": "GCS",
       "number": 20,
       "games": 3,
       "age": 18,
       "height": 202,
       "role": "FWD RCK"
    },
    "ethan stanley": {
       "first_name": "Ethan",
       "last_name": "Stanley",
       "current_team": "FRE",
       "number": 46,
       "games": 2,
       "age": 20,
       "height": 188,
       "role": "MID"
    },
    "finlay macrae": {
       "first_name": "Finlay",
       "last_name": "Macrae",
       "current_team": "COL",
       "number": 18,
       "games": 15,
       "age": 22,
       "height": 188,
       "role": "MID FWD"
    },
    "finn callaghan": {
       "first_name": "Finn",
       "last_name": "Callaghan",
       "current_team": "GWS",
       "number": 17,
       "games": 32,
       "age": 20,
       "height": 192,
       "role": "MID"
    },
    "finn maginness": {
       "first_name": "Finn",
       "last_name": "Maginness",
       "current_team": "HAW",
       "number": 32,
       "games": 37,
       "age": 23,
       "height": 189,
       "role": "MID FWD"
    },
    "francis evans": {
       "first_name": "Francis",
       "last_name": "Evans",
       "current_team": "POR",
       "number": 31,
       "games": 20,
       "age": 22,
       "height": 182,
       "role": "FWD"
    },
    "gary rohan": {
       "first_name": "Gary",
       "last_name": "Rohan",
       "current_team": "GEE",
       "number": 23,
       "games": 192,
       "age": 32,
       "height": 189,
       "role": "FWD"
    },
    "george hewett": {
       "first_name": "George",
       "last_name": "Hewett",
       "current_team": "CAR",
       "number": 29,
       "games": 163,
       "age": 28,
       "height": 187,
       "role": "MID"
    },
    "george wardlaw": {
       "first_name": "George",
       "last_name": "Wardlaw",
       "current_team": "NOR",
       "number": 6,
       "games": 13,
       "age": 19,
       "height": 182,
       "role": "MID"
    },
    "griffin logue": {
       "first_name": "Griffin",
       "last_name": "Logue",
       "current_team": "NOR",
       "number": 19,
       "games": 79,
       "age": 26,
       "height": 193,
       "role": "DEF"
    },
    "gryan miers": {
       "first_name": "Gryan",
       "last_name": "Miers",
       "current_team": "GEE",
       "number": 32,
       "games": 112,
       "age": 25,
       "height": 179,
       "role": "FWD"
    },
    "harley reid": {
       "first_name": "Harley",
       "last_name": "Reid",
       "current_team": "WCE",
       "number": 9,
       "games": 6,
       "age": 19,
       "height": 187,
       "role": "MID FWD"
    },
    "harris andrews": {
       "first_name": "Harris",
       "last_name": "Andrews",
       "current_team": "BRI",
       "number": 31,
       "games": 193,
       "age": 27,
       "height": 202,
       "role": "DEF"
    },
    "harrison himmelberg": {
       "first_name": "Harrison",
       "last_name": "Himmelberg",
       "current_team": "GWS",
       "number": 27,
       "games": 159,
       "age": 27,
       "height": 195,
       "role": "DEF"
    },
    "harrison jones": {
       "first_name": "Harrison",
       "last_name": "Jones",
       "current_team": "ESS",
       "number": 23,
       "games": 37,
       "age": 23,
       "height": 196,
       "role": "FWD"
    },
    "harrison petty": {
       "first_name": "Harrison",
       "last_name": "Petty",
       "current_team": "MEL",
       "number": 35,
       "games": 67,
       "age": 24,
       "height": 197,
       "role": "DEF FWD"
    },
    "harry barnett": {
       "first_name": "Harry",
       "last_name": "Barnett",
       "current_team": "WCE",
       "number": 30,
       "games": 2,
       "age": 20,
       "height": 203,
       "role": "FWD RCK"
    },
    "harry cunningham": {
       "first_name": "Harry",
       "last_name": "Cunningham",
       "current_team": "SYD",
       "number": 7,
       "games": 190,
       "age": 30,
       "height": 181,
       "role": "DEF"
    },
    "harry edwards": {
       "first_name": "Harry",
       "last_name": "Edwards",
       "current_team": "WCE",
       "number": 42,
       "games": 30,
       "age": 23,
       "height": 200,
       "role": "DEF"
    },
    "harry mckay": {
       "first_name": "Harry",
       "last_name": "McKay",
       "current_team": "CAR",
       "number": 10,
       "games": 113,
       "age": 26,
       "height": 200,
       "role": "FWD"
    },
    "harry morrison": {
       "first_name": "Harry",
       "last_name": "Morrison",
       "current_team": "HAW",
       "number": 1,
       "games": 93,
       "age": 25,
       "height": 184,
       "role": "MID"
    },
    "harry perryman": {
       "first_name": "Harry",
       "last_name": "Perryman",
       "current_team": "GWS",
       "number": 36,
       "games": 114,
       "age": 25,
       "height": 187,
       "role": "DEF"
    },
    "harry rowston": {
       "first_name": "Harry",
       "last_name": "Rowston",
       "current_team": "GWS",
       "number": 24,
       "games": 7,
       "age": 19,
       "height": 182,
       "role": "MID"
    },
    "harry schoenberg": {
       "first_name": "Harry",
       "last_name": "Schoenberg",
       "current_team": "ADE",
       "number": 26,
       "games": 57,
       "age": 23,
       "height": 183,
       "role": "MID"
    },
    "harry sharp": {
       "first_name": "Harry",
       "last_name": "Sharp",
       "current_team": "BRI",
       "number": 22,
       "games": 10,
       "age": 21,
       "height": 182,
       "role": "MID"
    },
    "harry sheezel": {
       "first_name": "Harry",
       "last_name": "Sheezel",
       "current_team": "NOR",
       "number": 3,
       "games": 29,
       "age": 19,
       "height": 185,
       "role": "DEF"
    },
    "harvey gallagher": {
       "first_name": "Harvey",
       "last_name": "Gallagher",
       "current_team": "WBD",
       "number": 12,
       "games": 6,
       "age": 20,
       "height": 181,
       "role": "MID FWD"
    },
    "harvey harrison": {
       "first_name": "Harvey",
       "last_name": "Harrison",
       "current_team": "COL",
       "number": 36,
       "games": 5,
       "age": 20,
       "height": 181,
       "role": "FWD"
    },
    "harvey thomas": {
       "first_name": "Harvey",
       "last_name": "Thomas",
       "current_team": "GWS",
       "number": 28,
       "games": 7,
       "age": 18,
       "height": 176,
       "role": "MID FWD"
    },
    "hayden mclean": {
       "first_name": "Hayden",
       "last_name": "McLean",
       "current_team": "SYD",
       "number": 2,
       "games": 59,
       "age": 25,
       "height": 197,
       "role": "FWD RCK"
    },
    "hayden young": {
       "first_name": "Hayden",
       "last_name": "Young",
       "current_team": "FRE",
       "number": 26,
       "games": 63,
       "age": 23,
       "height": 189,
       "role": "DEF MID"
    },
    "heath chapman": {
       "first_name": "Heath",
       "last_name": "Chapman",
       "current_team": "FRE",
       "number": 5,
       "games": 26,
       "age": 22,
       "height": 193,
       "role": "DEF"
    },
    "henry hustwaite": {
       "first_name": "Henry",
       "last_name": "Hustwaite",
       "current_team": "HAW",
       "number": 44,
       "games": 6,
       "age": 19,
       "height": 195,
       "role": "MID"
    },
    "hewago oea": {
       "first_name": "Hewago",
       "last_name": "Oea",
       "current_team": "GCS",
       "number": 47,
       "games": 13,
       "age": 22,
       "height": 177,
       "role": "MID FWD"
    },
    "hugh greenwood": {
       "first_name": "Hugh",
       "last_name": "Greenwood",
       "current_team": "NOR",
       "number": 1,
       "games": 120,
       "age": 32,
       "height": 190,
       "role": "MID"
    },
    "hugh mccluggage": {
       "first_name": "Hugh",
       "last_name": "McCluggage",
       "current_team": "BRI",
       "number": 6,
       "games": 162,
       "age": 26,
       "height": 185,
       "role": "MID"
    },
    "hugo garcia": {
       "first_name": "Hugo",
       "last_name": "Garcia",
       "current_team": "STK",
       "number": 34,
       "games": 2,
       "age": 18,
       "height": 185,
       "role": "MID FWD"
    },
    "hugo ralphsmith": {
       "first_name": "Hugo",
       "last_name": "Ralphsmith",
       "current_team": "RIC",
       "number": 13,
       "games": 34,
       "age": 22,
       "height": 187,
       "role": "DEF"
    },
    "hunter clark": {
       "first_name": "Hunter",
       "last_name": "Clark",
       "current_team": "STK",
       "number": 11,
       "games": 87,
       "age": 25,
       "height": 186,
       "role": "MID"
    },
    "isaac cumming": {
       "first_name": "Isaac",
       "last_name": "Cumming",
       "current_team": "GWS",
       "number": 13,
       "games": 75,
       "age": 25,
       "height": 188,
       "role": "DEF MID"
    },
    "isaac heeney": {
       "first_name": "Isaac",
       "last_name": "Heeney",
       "current_team": "SYD",
       "number": 5,
       "games": 183,
       "age": 27,
       "height": 185,
       "role": "MID FWD"
    },
    "isaac quaynor": {
       "first_name": "Isaac",
       "last_name": "Quaynor",
       "current_team": "COL",
       "number": 3,
       "games": 92,
       "age": 24,
       "height": 180,
       "role": "DEF"
    },
    "ivan soldo": {
       "first_name": "Ivan",
       "last_name": "Soldo",
       "current_team": "POR",
       "number": 13,
       "games": 63,
       "age": 28,
       "height": 204,
       "role": "RCK"
    },
    "izak rankine": {
       "first_name": "Izak",
       "last_name": "Rankine",
       "current_team": "ADE",
       "number": 23,
       "games": 74,
       "age": 24,
       "height": 181,
       "role": "FWD"
    },
    "jack billings": {
       "first_name": "Jack",
       "last_name": "Billings",
       "current_team": "MEL",
       "number": 14,
       "games": 162,
       "age": 28,
       "height": 185,
       "role": "MID FWD"
    },
    "jack bowes": {
       "first_name": "Jack",
       "last_name": "Bowes",
       "current_team": "GEE",
       "number": 12,
       "games": 104,
       "age": 26,
       "height": 189,
       "role": "DEF MID"
    },
    "jack buckley": {
       "first_name": "Jack",
       "last_name": "Buckley",
       "current_team": "GWS",
       "number": 44,
       "games": 50,
       "age": 26,
       "height": 195,
       "role": "DEF"
    },
    "jack buller": {
       "first_name": "Jack",
       "last_name": "Buller",
       "current_team": "SYD",
       "number": 39,
       "games": 1,
       "age": 22,
       "height": 199,
       "role": "FWD"
    },
    "jack bytel": {
       "first_name": "Jack",
       "last_name": "Bytel",
       "current_team": "COL",
       "number": 27,
       "games": 22,
       "age": 24,
       "height": 189,
       "role": "MID"
    },
    "jack carroll": {
       "first_name": "Jack",
       "last_name": "Carroll",
       "current_team": "CAR",
       "number": 16,
       "games": 12,
       "age": 21,
       "height": 188,
       "role": "MID"
    },
    "jack crisp": {
       "first_name": "Jack",
       "last_name": "Crisp",
       "current_team": "COL",
       "number": 25,
       "games": 233,
       "age": 30,
       "height": 190,
       "role": "DEF MID"
    },
    "jack darling": {
       "first_name": "Jack",
       "last_name": "Darling",
       "current_team": "WCE",
       "number": 27,
       "games": 283,
       "age": 31,
       "height": 191,
       "role": "FWD"
    },
    "jack ginnivan": {
       "first_name": "Jack",
       "last_name": "Ginnivan",
       "current_team": "HAW",
       "number": 33,
       "games": 48,
       "age": 21,
       "height": 185,
       "role": "FWD"
    },
    "jack graham": {
       "first_name": "Jack",
       "last_name": "Graham",
       "current_team": "RIC",
       "number": 34,
       "games": 120,
       "age": 26,
       "height": 181,
       "role": "FWD"
    },
    "jack gunston": {
       "first_name": "Jack",
       "last_name": "Gunston",
       "current_team": "HAW",
       "number": 19,
       "games": 245,
       "age": 32,
       "height": 193,
       "role": "FWD"
    },
    "jack hayes": {
       "first_name": "Jack",
       "last_name": "Hayes",
       "current_team": "STK",
       "number": 18,
       "games": 6,
       "age": 28,
       "height": 194,
       "role": "FWD"
    },
    "jack henry": {
       "first_name": "Jack",
       "last_name": "Henry",
       "current_team": "GEE",
       "number": 38,
       "games": 124,
       "age": 25,
       "height": 192,
       "role": "DEF"
    },
    "jack higgins": {
       "first_name": "Jack",
       "last_name": "Higgins",
       "current_team": "STK",
       "number": 1,
       "games": 108,
       "age": 25,
       "height": 178,
       "role": "FWD"
    },
    "jack lukosius": {
       "first_name": "Jack",
       "last_name": "Lukosius",
       "current_team": "GCS",
       "number": 13,
       "games": 101,
       "age": 23,
       "height": 195,
       "role": "DEF FWD"
    },
    "jack mahony": {
       "first_name": "Jack",
       "last_name": "Mahony",
       "current_team": "GCS",
       "number": 42,
       "games": 44,
       "age": 22,
       "height": 177,
       "role": "MID FWD"
    },
    "jack martin": {
       "first_name": "Jack",
       "last_name": "Martin",
       "current_team": "CAR",
       "number": 21,
       "games": 148,
       "age": 29,
       "height": 186,
       "role": "FWD"
    },
    "jack payne": {
       "first_name": "Jack",
       "last_name": "Payne",
       "current_team": "BRI",
       "number": 40,
       "games": 56,
       "age": 24,
       "height": 197,
       "role": "DEF"
    },
    "jack petruccelle": {
       "first_name": "Jack",
       "last_name": "Petruccelle",
       "current_team": "WCE",
       "number": 21,
       "games": 78,
       "age": 25,
       "height": 188,
       "role": "FWD"
    },
    "jack ross": {
       "first_name": "Jack",
       "last_name": "Ross",
       "current_team": "RIC",
       "number": 5,
       "games": 68,
       "age": 23,
       "height": 187,
       "role": "MID"
    },
    "jack scrimshaw": {
       "first_name": "Jack",
       "last_name": "Scrimshaw",
       "current_team": "HAW",
       "number": 14,
       "games": 89,
       "age": 25,
       "height": 194,
       "role": "DEF"
    },
    "jack silvagni": {
       "first_name": "Jack",
       "last_name": "Silvagni",
       "current_team": "CAR",
       "number": 1,
       "games": 115,
       "age": 26,
       "height": 194,
       "role": "FWD"
    },
    "jack sinclair": {
       "first_name": "Jack",
       "last_name": "Sinclair",
       "current_team": "STK",
       "number": 35,
       "games": 170,
       "age": 29,
       "height": 181,
       "role": "DEF"
    },
    "jack steele": {
       "first_name": "Jack",
       "last_name": "Steele",
       "current_team": "STK",
       "number": 9,
       "games": 164,
       "age": 28,
       "height": 187,
       "role": "MID"
    },
    "jack viney": {
       "first_name": "Jack",
       "last_name": "Viney",
       "current_team": "MEL",
       "number": 7,
       "games": 203,
       "age": 30,
       "height": 179,
       "role": "MID"
    },
    "jack williams": {
       "first_name": "Jack",
       "last_name": "Williams",
       "current_team": "WCE",
       "number": 34,
       "games": 15,
       "age": 20,
       "height": 198,
       "role": "FWD"
    },
    "jackson archer": {
       "first_name": "Jackson",
       "last_name": "Archer",
       "current_team": "NOR",
       "number": 34,
       "games": 8,
       "age": 21,
       "height": 187,
       "role": "DEF"
    },
    "jackson macrae": {
       "first_name": "Jackson",
       "last_name": "Macrae",
       "current_team": "WBD",
       "number": 11,
       "games": 234,
       "age": 29,
       "height": 192,
       "role": "MID FWD"
    },
    "jackson mead": {
       "first_name": "Jackson",
       "last_name": "Mead",
       "current_team": "POR",
       "number": 44,
       "games": 24,
       "age": 22,
       "height": 184,
       "role": "MID FWD"
    },
    "jacob bauer": {
       "first_name": "Jacob",
       "last_name": "Bauer",
       "current_team": "RIC",
       "number": 43,
       "games": 4,
       "age": 21,
       "height": 192,
       "role": "FWD"
    },
    "jacob hopper": {
       "first_name": "Jacob",
       "last_name": "Hopper",
       "current_team": "RIC",
       "number": 22,
       "games": 133,
       "age": 27,
       "height": 187,
       "role": "MID"
    },
    "jacob koschitzke": {
       "first_name": "Jacob",
       "last_name": "Koschitzke",
       "current_team": "RIC",
       "number": 20,
       "games": 51,
       "age": 23,
       "height": 196,
       "role": "FWD"
    },
    "jacob van rooyen": {
       "first_name": "Jacob",
       "last_name": "Van Rooyen",
       "current_team": "MEL",
       "number": 2,
       "games": 27,
       "age": 21,
       "height": 193,
       "role": "FWD"
    },
    "jacob wehr": {
       "first_name": "Jacob",
       "last_name": "Wehr",
       "current_team": "GWS",
       "number": 10,
       "games": 21,
       "age": 25,
       "height": 185,
       "role": "MID"
    },
    "jacob weitering": {
       "first_name": "Jacob",
       "last_name": "Weitering",
       "current_team": "CAR",
       "number": 23,
       "games": 163,
       "age": 26,
       "height": 195,
       "role": "DEF"
    },
    "jade gresham": {
       "first_name": "Jade",
       "last_name": "Gresham",
       "current_team": "ESS",
       "number": 11,
       "games": 143,
       "age": 26,
       "height": 177,
       "role": "FWD"
    },
    "jaeger o'meara": {
       "first_name": "Jaeger",
       "last_name": "O'Meara",
       "current_team": "FRE",
       "number": 2,
       "games": 169,
       "age": 30,
       "height": 184,
       "role": "MID"
    },
    "jai culley": {
       "first_name": "Jai",
       "last_name": "Culley",
       "current_team": "WCE",
       "number": 49,
       "games": 9,
       "age": 21,
       "height": 194,
       "role": "MID FWD"
    },
    "jai newcombe": {
       "first_name": "Jai",
       "last_name": "Newcombe",
       "current_team": "HAW",
       "number": 3,
       "games": 57,
       "age": 22,
       "height": 187,
       "role": "MID"
    },
    "jai serong": {
       "first_name": "Jai",
       "last_name": "Serong",
       "current_team": "HAW",
       "number": 29,
       "games": 6,
       "age": 21,
       "height": 193,
       "role": "DEF"
    },
    "jaidyn stephenson": {
       "first_name": "Jaidyn",
       "last_name": "Stephenson",
       "current_team": "NOR",
       "number": 2,
       "games": 114,
       "age": 25,
       "height": 189,
       "role": "FWD"
    },
    "jake bowey": {
       "first_name": "Jake",
       "last_name": "Bowey",
       "current_team": "MEL",
       "number": 17,
       "games": 48,
       "age": 21,
       "height": 175,
       "role": "DEF"
    },
    "jake kelly": {
       "first_name": "Jake",
       "last_name": "Kelly",
       "current_team": "ESS",
       "number": 29,
       "games": 154,
       "age": 29,
       "height": 190,
       "role": "DEF MID"
    },
    "jake kolodjashnij": {
       "first_name": "Jake",
       "last_name": "Kolodjashnij",
       "current_team": "GEE",
       "number": 8,
       "games": 179,
       "age": 28,
       "height": 193,
       "role": "DEF"
    },
    "jake lever": {
       "first_name": "Jake",
       "last_name": "Lever",
       "current_team": "MEL",
       "number": 8,
       "games": 167,
       "age": 28,
       "height": 194,
       "role": "DEF"
    },
    "jake lloyd": {
       "first_name": "Jake",
       "last_name": "Lloyd",
       "current_team": "SYD",
       "number": 44,
       "games": 229,
       "age": 30,
       "height": 182,
       "role": "DEF MID"
    },
    "jake melksham": {
       "first_name": "Jake",
       "last_name": "Melksham",
       "current_team": "MEL",
       "number": 18,
       "games": 221,
       "age": 32,
       "height": 186,
       "role": "FWD"
    },
    "jake riccardi": {
       "first_name": "Jake",
       "last_name": "Riccardi",
       "current_team": "GWS",
       "number": 26,
       "games": 57,
       "age": 24,
       "height": 198,
       "role": "FWD"
    },
    "jake rogers": {
       "first_name": "Jake",
       "last_name": "Rogers",
       "current_team": "GCS",
       "number": 29,
       "games": 1,
       "age": 19,
       "height": 172,
       "role": "MID"
    },
    "jake soligo": {
       "first_name": "Jake",
       "last_name": "Soligo",
       "current_team": "ADE",
       "number": 14,
       "games": 43,
       "age": 21,
       "height": 180,
       "role": "MID"
    },
    "jake stringer": {
       "first_name": "Jake",
       "last_name": "Stringer",
       "current_team": "ESS",
       "number": 25,
       "games": 196,
       "age": 30,
       "height": 192,
       "role": "FWD"
    },
    "jake waterman": {
       "first_name": "Jake",
       "last_name": "Waterman",
       "current_team": "WCE",
       "number": 2,
       "games": 90,
       "age": 25,
       "height": 192,
       "role": "FWD"
    },
    "jakob ryan": {
       "first_name": "Jakob",
       "last_name": "Ryan",
       "current_team": "COL",
       "number": 24,
       "games": 1,
       "age": 19,
       "height": 189,
       "role": "DEF"
    },
    "jamaine jones": {
       "first_name": "Jamaine",
       "last_name": "Jones",
       "current_team": "WCE",
       "number": 31,
       "games": 57,
       "age": 25,
       "height": 180,
       "role": "DEF"
    },
    "jamarra ugle-hagan": {
       "first_name": "Jamarra",
       "last_name": "Ugle-Hagan",
       "current_team": "WBD",
       "number": 2,
       "games": 50,
       "age": 22,
       "height": 197,
       "role": "FWD"
    },
    "james aish": {
       "first_name": "James",
       "last_name": "Aish",
       "current_team": "FRE",
       "number": 11,
       "games": 169,
       "age": 28,
       "height": 183,
       "role": "DEF MID"
    },
    "james blanck": {
       "first_name": "James",
       "last_name": "Blanck",
       "current_team": "HAW",
       "number": 36,
       "games": 24,
       "age": 23,
       "height": 196,
       "role": "DEF"
    },
    "james borlase": {
       "first_name": "James",
       "last_name": "Borlase",
       "current_team": "ADE",
       "number": 35,
       "games": 7,
       "age": 21,
       "height": 192,
       "role": "DEF"
    },
    "james harmes": {
       "first_name": "James",
       "last_name": "Harmes",
       "current_team": "WBD",
       "number": 22,
       "games": 154,
       "age": 28,
       "height": 186,
       "role": "MID FWD"
    },
    "james jordon": {
       "first_name": "James",
       "last_name": "Jordon",
       "current_team": "SYD",
       "number": 17,
       "games": 71,
       "age": 23,
       "height": 187,
       "role": "MID FWD"
    },
    "james madden": {
       "first_name": "James",
       "last_name": "Madden",
       "current_team": "BRI",
       "number": 14,
       "games": 13,
       "age": 24,
       "height": 188,
       "role": "DEF"
    },
    "james o'donnell": {
       "first_name": "James",
       "last_name": "O'Donnell",
       "current_team": "WBD",
       "number": 18,
       "games": 14,
       "age": 21,
       "height": 197,
       "role": "DEF"
    },
    "james peatling": {
       "first_name": "James",
       "last_name": "Peatling",
       "current_team": "GWS",
       "number": 20,
       "games": 31,
       "age": 23,
       "height": 186,
       "role": "MID FWD"
    },
    "james rowbottom": {
       "first_name": "James",
       "last_name": "Rowbottom",
       "current_team": "SYD",
       "number": 8,
       "games": 99,
       "age": 23,
       "height": 186,
       "role": "MID"
    },
    "james sicily": {
       "first_name": "James",
       "last_name": "Sicily",
       "current_team": "HAW",
       "number": 6,
       "games": 140,
       "age": 29,
       "height": 188,
       "role": "DEF"
    },
    "james trezise": {
       "first_name": "James",
       "last_name": "Trezise",
       "current_team": "RIC",
       "number": 36,
       "games": 2,
       "age": 21,
       "height": 188,
       "role": "DEF"
    },
    "james tsitas": {
       "first_name": "James",
       "last_name": "Tsitas",
       "current_team": "GCS",
       "number": 21,
       "games": 5,
       "age": 29,
       "height": 181,
       "role": "FWD"
    },
    "james tunstill": {
       "first_name": "James",
       "last_name": "Tunstill",
       "current_team": "BRI",
       "number": 29,
       "games": 9,
       "age": 20,
       "height": 187,
       "role": "FWD"
    },
    "james worpel": {
       "first_name": "James",
       "last_name": "Worpel",
       "current_team": "HAW",
       "number": 5,
       "games": 108,
       "age": 25,
       "height": 186,
       "role": "MID"
    },
    "jamie cripps": {
       "first_name": "Jamie",
       "last_name": "Cripps",
       "current_team": "WCE",
       "number": 15,
       "games": 234,
       "age": 32,
       "height": 184,
       "role": "FWD"
    },
    "jamie elliott": {
       "first_name": "Jamie",
       "last_name": "Elliott",
       "current_team": "COL",
       "number": 5,
       "games": 186,
       "age": 31,
       "height": 178,
       "role": "FWD"
    },
    "jarman impey": {
       "first_name": "Jarman",
       "last_name": "Impey",
       "current_team": "HAW",
       "number": 4,
       "games": 178,
       "age": 28,
       "height": 178,
       "role": "DEF"
    },
    "jarrod berry": {
       "first_name": "Jarrod",
       "last_name": "Berry",
       "current_team": "BRI",
       "number": 7,
       "games": 140,
       "age": 26,
       "height": 192,
       "role": "MID"
    },
    "jarrod witts": {
       "first_name": "Jarrod",
       "last_name": "Witts",
       "current_team": "GCS",
       "number": 28,
       "games": 170,
       "age": 31,
       "height": 209,
       "role": "RCK"
    },
    "jarryd lyons": {
       "first_name": "Jarryd",
       "last_name": "Lyons",
       "current_team": "BRI",
       "number": 17,
       "games": 194,
       "age": 31,
       "height": 184,
       "role": "MID"
    },
    "jase burgoyne": {
       "first_name": "Jase",
       "last_name": "Burgoyne",
       "current_team": "POR",
       "number": 7,
       "games": 18,
       "age": 20,
       "height": 186,
       "role": "DEF"
    },
    "jason horne-francis": {
       "first_name": "Jason",
       "last_name": "Horne-Francis",
       "current_team": "POR",
       "number": 18,
       "games": 45,
       "age": 20,
       "height": 185,
       "role": "MID"
    },
    "jason johannisen": {
       "first_name": "Jason",
       "last_name": "Johannisen",
       "current_team": "WBD",
       "number": 39,
       "games": 193,
       "age": 31,
       "height": 181,
       "role": "DEF"
    },
    "jaspa fletcher": {
       "first_name": "Jaspa",
       "last_name": "Fletcher",
       "current_team": "BRI",
       "number": 28,
       "games": 21,
       "age": 20,
       "height": 183,
       "role": "MID"
    },
    "jaxon prior": {
       "first_name": "Jaxon",
       "last_name": "Prior",
       "current_team": "BRI",
       "number": 20,
       "games": 35,
       "age": 22,
       "height": 189,
       "role": "DEF"
    },
    "jayden hunt": {
       "first_name": "Jayden",
       "last_name": "Hunt",
       "current_team": "WCE",
       "number": 5,
       "games": 143,
       "age": 29,
       "height": 188,
       "role": "DEF MID"
    },
    "jayden laverde": {
       "first_name": "Jayden",
       "last_name": "Laverde",
       "current_team": "ESS",
       "number": 15,
       "games": 116,
       "age": 28,
       "height": 193,
       "role": "DEF"
    },
    "jayden short": {
       "first_name": "Jayden",
       "last_name": "Short",
       "current_team": "RIC",
       "number": 15,
       "games": 159,
       "age": 28,
       "height": 178,
       "role": "DEF"
    },
    "jed bews": {
       "first_name": "Jed",
       "last_name": "Bews",
       "current_team": "GEE",
       "number": 24,
       "games": 166,
       "age": 30,
       "height": 186,
       "role": "DEF"
    },
    "jed mcentee": {
       "first_name": "Jed",
       "last_name": "McEntee",
       "current_team": "POR",
       "number": 41,
       "games": 37,
       "age": 23,
       "height": 176,
       "role": "FWD"
    },
    "jed walter": {
       "first_name": "Jed",
       "last_name": "Walter",
       "current_team": "GCS",
       "number": 17,
       "games": 4,
       "age": 18,
       "height": 195,
       "role": "FWD"
    },
    "jeremy cameron": {
       "first_name": "Jeremy",
       "last_name": "Cameron",
       "current_team": "GEE",
       "number": 5,
       "games": 236,
       "age": 31,
       "height": 196,
       "role": "FWD"
    },
    "jeremy finlayson": {
       "first_name": "Jeremy",
       "last_name": "Finlayson",
       "current_team": "POR",
       "number": 11,
       "games": 112,
       "age": 28,
       "height": 197,
       "role": "FWD"
    },
    "jeremy howe": {
       "first_name": "Jeremy",
       "last_name": "Howe",
       "current_team": "COL",
       "number": 38,
       "games": 239,
       "age": 33,
       "height": 190,
       "role": "DEF"
    },
    "jeremy mcgovern": {
       "first_name": "Jeremy",
       "last_name": "McGovern",
       "current_team": "WCE",
       "number": 20,
       "games": 178,
       "age": 32,
       "height": 197,
       "role": "DEF"
    },
    "jeremy sharp": {
       "first_name": "Jeremy",
       "last_name": "Sharp",
       "current_team": "FRE",
       "number": 14,
       "games": 29,
       "age": 22,
       "height": 189,
       "role": "MID"
    },
    "jesse hogan": {
       "first_name": "Jesse",
       "last_name": "Hogan",
       "current_team": "GWS",
       "number": 23,
       "games": 147,
       "age": 29,
       "height": 196,
       "role": "FWD"
    },
    "jesse motlop": {
       "first_name": "Jesse",
       "last_name": "Motlop",
       "current_team": "CAR",
       "number": 3,
       "games": 33,
       "age": 20,
       "height": 180,
       "role": "FWD"
    },
    "jhye clark": {
       "first_name": "Jhye",
       "last_name": "Clark",
       "current_team": "GEE",
       "number": 13,
       "games": 7,
       "age": 19,
       "height": 181,
       "role": "MID"
    },
    "jimmy webster": {
       "first_name": "Jimmy",
       "last_name": "Webster",
       "current_team": "STK",
       "number": 29,
       "games": 150,
       "age": 30,
       "height": 188,
       "role": "DEF"
    },
    "joe daniher": {
       "first_name": "Joe",
       "last_name": "Daniher",
       "current_team": "BRI",
       "number": 3,
       "games": 184,
       "age": 30,
       "height": 201,
       "role": "FWD"
    },
    "joel amartey": {
       "first_name": "Joel",
       "last_name": "Amartey",
       "current_team": "SYD",
       "number": 36,
       "games": 34,
       "age": 24,
       "height": 195,
       "role": "FWD"
    },
    "joel hamling": {
       "first_name": "Joel",
       "last_name": "Hamling",
       "current_team": "SYD",
       "number": 29,
       "games": 91,
       "age": 31,
       "height": 194,
       "role": "DEF"
    },
    "joel jeffrey": {
       "first_name": "Joel",
       "last_name": "Jeffrey",
       "current_team": "GCS",
       "number": 40,
       "games": 16,
       "age": 22,
       "height": 192,
       "role": "DEF FWD"
    },
    "joel smith": {
       "first_name": "Joel",
       "last_name": "Smith",
       "current_team": "MEL",
       "number": 29,
       "games": 42,
       "age": 28,
       "height": 191,
       "role": "FWD"
    },
    "john noble": {
       "first_name": "John",
       "last_name": "Noble",
       "current_team": "COL",
       "number": 9,
       "games": 97,
       "age": 27,
       "height": 180,
       "role": "DEF"
    },
    "jordan boyd": {
       "first_name": "Jordan",
       "last_name": "Boyd",
       "current_team": "CAR",
       "number": 37,
       "games": 22,
       "age": 25,
       "height": 182,
       "role": "DEF"
    },
    "jordan clark": {
       "first_name": "Jordan",
       "last_name": "Clark",
       "current_team": "FRE",
       "number": 6,
       "games": 85,
       "age": 23,
       "height": 185,
       "role": "DEF"
    },
    "jordan dawson": {
       "first_name": "Jordan",
       "last_name": "Dawson",
       "current_team": "ADE",
       "number": 12,
       "games": 115,
       "age": 27,
       "height": 191,
       "role": "MID"
    },
    "jordan de goey": {
       "first_name": "Jordan",
       "last_name": "De Goey",
       "current_team": "COL",
       "number": 2,
       "games": 165,
       "age": 28,
       "height": 188,
       "role": "MID"
    },
    "jordan ridley": {
       "first_name": "Jordan",
       "last_name": "Ridley",
       "current_team": "ESS",
       "number": 14,
       "games": 85,
       "age": 25,
       "height": 195,
       "role": "DEF"
    },
    "jordon butts": {
       "first_name": "Jordon",
       "last_name": "Butts",
       "current_team": "ADE",
       "number": 41,
       "games": 64,
       "age": 24,
       "height": 198,
       "role": "DEF"
    },
    "jordon sweet": {
       "first_name": "Jordon",
       "last_name": "Sweet",
       "current_team": "POR",
       "number": 24,
       "games": 12,
       "age": 26,
       "height": 206,
       "role": "RCK"
    },
    "josh battle": {
       "first_name": "Josh",
       "last_name": "Battle",
       "current_team": "STK",
       "number": 26,
       "games": 106,
       "age": 25,
       "height": 193,
       "role": "DEF"
    },
    "josh carmichael": {
       "first_name": "Josh",
       "last_name": "Carmichael",
       "current_team": "COL",
       "number": 12,
       "games": 8,
       "age": 24,
       "height": 190,
       "role": "FWD"
    },
    "josh corbett": {
       "first_name": "Josh",
       "last_name": "Corbett",
       "current_team": "FRE",
       "number": 19,
       "games": 41,
       "age": 28,
       "height": 190,
       "role": "FWD"
    },
    "josh daicos": {
       "first_name": "Josh",
       "last_name": "Daicos",
       "current_team": "COL",
       "number": 7,
       "games": 110,
       "age": 25,
       "height": 178,
       "role": "MID"
    },
    "josh draper": {
       "first_name": "Josh",
       "last_name": "Draper",
       "current_team": "FRE",
       "number": 37,
       "games": 5,
       "age": 20,
       "height": 197,
       "role": "DEF"
    },
    "josh dunkley": {
       "first_name": "Josh",
       "last_name": "Dunkley",
       "current_team": "BRI",
       "number": 5,
       "games": 147,
       "age": 27,
       "height": 191,
       "role": "MID"
    },
    "josh fahey": {
       "first_name": "Josh",
       "last_name": "Fahey",
       "current_team": "GWS",
       "number": 34,
       "games": 7,
       "age": 20,
       "height": 187,
       "role": "MID"
    },
    "josh gibcus": {
       "first_name": "Josh",
       "last_name": "Gibcus",
       "current_team": "RIC",
       "number": 18,
       "games": 20,
       "age": 21,
       "height": 196,
       "role": "DEF"
    },
    "josh goater": {
       "first_name": "Josh",
       "last_name": "Goater",
       "current_team": "NOR",
       "number": 31,
       "games": 12,
       "age": 20,
       "height": 190,
       "role": "DEF"
    },
    "josh rachele": {
       "first_name": "Josh",
       "last_name": "Rachele",
       "current_team": "ADE",
       "number": 8,
       "games": 40,
       "age": 21,
       "height": 179,
       "role": "FWD"
    },
    "josh rotham": {
       "first_name": "Josh",
       "last_name": "Rotham",
       "current_team": "WCE",
       "number": 35,
       "games": 62,
       "age": 26,
       "height": 193,
       "role": "DEF"
    },
    "josh schache": {
       "first_name": "Josh",
       "last_name": "Schache",
       "current_team": "MEL",
       "number": 19,
       "games": 76,
       "age": 26,
       "height": 199,
       "role": "FWD"
    },
    "josh sinn": {
       "first_name": "Josh",
       "last_name": "Sinn",
       "current_team": "POR",
       "number": 8,
       "games": 4,
       "age": 21,
       "height": 187,
       "role": "MID"
    },
    "josh treacy": {
       "first_name": "Josh",
       "last_name": "Treacy",
       "current_team": "FRE",
       "number": 35,
       "games": 42,
       "age": 21,
       "height": 195,
       "role": "FWD"
    },
    "josh ward": {
       "first_name": "Josh",
       "last_name": "Ward",
       "current_team": "HAW",
       "number": 25,
       "games": 35,
       "age": 20,
       "height": 183,
       "role": "MID"
    },
    "josh worrell": {
       "first_name": "Josh",
       "last_name": "Worrell",
       "current_team": "ADE",
       "number": 24,
       "games": 23,
       "age": 23,
       "height": 195,
       "role": "DEF"
    },
    "joshua kelly": {
       "first_name": "Joshua",
       "last_name": "Kelly",
       "current_team": "GWS",
       "number": 22,
       "games": 205,
       "age": 29,
       "height": 184,
       "role": "MID"
    },
    "joshua weddle": {
       "first_name": "Joshua",
       "last_name": "Weddle",
       "current_team": "HAW",
       "number": 23,
       "games": 23,
       "age": 19,
       "height": 191,
       "role": "DEF"
    },
    "judd mcvee": {
       "first_name": "Judd",
       "last_name": "McVee",
       "current_team": "MEL",
       "number": 4,
       "games": 32,
       "age": 20,
       "height": 185,
       "role": "DEF"
    },
    "judson clarke": {
       "first_name": "Judson",
       "last_name": "Clarke",
       "current_team": "RIC",
       "number": 23,
       "games": 17,
       "age": 20,
       "height": 180,
       "role": "FWD"
    },
    "justin mcinerney": {
       "first_name": "Justin",
       "last_name": "McInerney",
       "current_team": "SYD",
       "number": 27,
       "games": 78,
       "age": 23,
       "height": 189,
       "role": "MID FWD"
    },
    "jy farrar": {
       "first_name": "Jy",
       "last_name": "Farrar",
       "current_team": "GCS",
       "number": 50,
       "games": 34,
       "age": 27,
       "height": 190,
       "role": "DEF"
    },
    "jy simpkin": {
       "first_name": "Jy",
       "last_name": "Simpkin",
       "current_team": "NOR",
       "number": 12,
       "games": 139,
       "age": 26,
       "height": 183,
       "role": "MID FWD"
    },
    "jye amiss": {
       "first_name": "Jye",
       "last_name": "Amiss",
       "current_team": "FRE",
       "number": 24,
       "games": 31,
       "age": 20,
       "height": 196,
       "role": "FWD"
    },
    "jye caldwell": {
       "first_name": "Jye",
       "last_name": "Caldwell",
       "current_team": "ESS",
       "number": 6,
       "games": 62,
       "age": 23,
       "height": 183,
       "role": "MID FWD"
    },
    "jye menzie": {
       "first_name": "Jye",
       "last_name": "Menzie",
       "current_team": "ESS",
       "number": 47,
       "games": 28,
       "age": 21,
       "height": 180,
       "role": "FWD"
    },
    "kade chandler": {
       "first_name": "Kade",
       "last_name": "Chandler",
       "current_team": "MEL",
       "number": 37,
       "games": 40,
       "age": 24,
       "height": 175,
       "role": "FWD"
    },
    "kai lohmann": {
       "first_name": "Kai",
       "last_name": "Lohmann",
       "current_team": "BRI",
       "number": 1,
       "games": 15,
       "age": 20,
       "height": 185,
       "role": "FWD"
    },
    "kaine baldwin": {
       "first_name": "Kaine",
       "last_name": "Baldwin",
       "current_team": "ESS",
       "number": 26,
       "games": 8,
       "age": 21,
       "height": 193,
       "role": "DEF"
    },
    "kallan dawson": {
       "first_name": "Kallan",
       "last_name": "Dawson",
       "current_team": "NOR",
       "number": 42,
       "games": 9,
       "age": 25,
       "height": 196,
       "role": "DEF"
    },
    "kamdyn mcintosh": {
       "first_name": "Kamdyn",
       "last_name": "McIntosh",
       "current_team": "RIC",
       "number": 33,
       "games": 175,
       "age": 30,
       "height": 191,
       "role": "MID"
    },
    "kane farrell": {
       "first_name": "Kane",
       "last_name": "Farrell",
       "current_team": "POR",
       "number": 6,
       "games": 77,
       "age": 25,
       "height": 182,
       "role": "DEF"
    },
    "kane mcauliffe": {
       "first_name": "Kane",
       "last_name": "McAuliffe",
       "current_team": "RIC",
       "number": 28,
       "games": 2,
       "age": 19,
       "height": 187,
       "role": "MID"
    },
    "karl amon": {
       "first_name": "Karl",
       "last_name": "Amon",
       "current_team": "HAW",
       "number": 10,
       "games": 151,
       "age": 28,
       "height": 181,
       "role": "DEF MID"
    },
    "karl worner": {
       "first_name": "Karl",
       "last_name": "Worner",
       "current_team": "FRE",
       "number": 23,
       "games": 5,
       "age": 21,
       "height": 188,
       "role": "DEF"
    },
    "keidean coleman": {
       "first_name": "Keidean",
       "last_name": "Coleman",
       "current_team": "BRI",
       "number": 18,
       "games": 65,
       "age": 24,
       "height": 182,
       "role": "DEF"
    },
    "kieran strachan": {
       "first_name": "Kieran",
       "last_name": "Strachan",
       "current_team": "ADE",
       "number": 45,
       "games": 5,
       "age": 28,
       "height": 204,
       "role": "RCK"
    },
    "kieren briggs": {
       "first_name": "Kieren",
       "last_name": "Briggs",
       "current_team": "GWS",
       "number": 32,
       "games": 33,
       "age": 24,
       "height": 201,
       "role": "RCK"
    },
    "koltyn tholstrup": {
       "first_name": "Koltyn",
       "last_name": "Tholstrup",
       "current_team": "MEL",
       "number": 39,
       "games": 1,
       "age": 18,
       "height": 186,
       "role": "FWD"
    },
    "kyle langford": {
       "first_name": "Kyle",
       "last_name": "Langford",
       "current_team": "ESS",
       "number": 4,
       "games": 137,
       "age": 27,
       "height": 192,
       "role": "FWD"
    },
    "kysaiah pickett": {
       "first_name": "Kysaiah",
       "last_name": "Pickett",
       "current_team": "MEL",
       "number": 36,
       "games": 90,
       "age": 22,
       "height": 171,
       "role": "FWD"
    },
    "lachie neale": {
       "first_name": "Lachie",
       "last_name": "Neale",
       "current_team": "BRI",
       "number": 9,
       "games": 252,
       "age": 30,
       "height": 178,
       "role": "MID"
    },
    "lachie whitfield": {
       "first_name": "Lachie",
       "last_name": "Whitfield",
       "current_team": "GWS",
       "number": 6,
       "games": 216,
       "age": 29,
       "height": 185,
       "role": "DEF"
    },
    "lachlan ash": {
       "first_name": "Lachlan",
       "last_name": "Ash",
       "current_team": "GWS",
       "number": 7,
       "games": 89,
       "age": 22,
       "height": 188,
       "role": "DEF"
    },
    "lachlan bramble": {
       "first_name": "Lachlan",
       "last_name": "Bramble",
       "current_team": "WBD",
       "number": 29,
       "games": 36,
       "age": 26,
       "height": 182,
       "role": "DEF MID"
    },
    "lachlan cowan": {
       "first_name": "Lachlan",
       "last_name": "Cowan",
       "current_team": "CAR",
       "number": 2,
       "games": 8,
       "age": 19,
       "height": 188,
       "role": "DEF"
    },
    "lachlan fogarty": {
       "first_name": "Lachlan",
       "last_name": "Fogarty",
       "current_team": "CAR",
       "number": 8,
       "games": 59,
       "age": 25,
       "height": 180,
       "role": "FWD"
    },
    "lachlan gollant": {
       "first_name": "Lachlan",
       "last_name": "Gollant",
       "current_team": "ADE",
       "number": 44,
       "games": 15,
       "age": 22,
       "height": 193,
       "role": "FWD"
    },
    "lachlan hunter": {
       "first_name": "Lachlan",
       "last_name": "Hunter",
       "current_team": "MEL",
       "number": 12,
       "games": 197,
       "age": 29,
       "height": 183,
       "role": "MID"
    },
    "lachlan jones": {
       "first_name": "Lachlan",
       "last_name": "Jones",
       "current_team": "POR",
       "number": 34,
       "games": 41,
       "age": 22,
       "height": 186,
       "role": "DEF"
    },
    "lachlan keeffe": {
       "first_name": "Lachlan",
       "last_name": "Keeffe",
       "current_team": "GWS",
       "number": 25,
       "games": 111,
       "age": 34,
       "height": 204,
       "role": "FWD"
    },
    "lachlan mcandrew": {
       "first_name": "Lachlan",
       "last_name": "McAndrew",
       "current_team": "SYD",
       "number": 46,
       "games": 2,
       "age": 23,
       "height": 209,
       "role": "RCK"
    },
    "lachlan mcneil": {
       "first_name": "Lachlan",
       "last_name": "McNeil",
       "current_team": "WBD",
       "number": 30,
       "games": 43,
       "age": 22,
       "height": 184,
       "role": "FWD"
    },
    "lachlan murphy": {
       "first_name": "Lachlan",
       "last_name": "Murphy",
       "current_team": "ADE",
       "number": 4,
       "games": 101,
       "age": 25,
       "height": 174,
       "role": "FWD"
    },
    "lachlan schultz": {
       "first_name": "Lachlan",
       "last_name": "Schultz",
       "current_team": "COL",
       "number": 8,
       "games": 97,
       "age": 26,
       "height": 178,
       "role": "FWD"
    },
    "lachlan sholl": {
       "first_name": "Lachlan",
       "last_name": "Sholl",
       "current_team": "ADE",
       "number": 38,
       "games": 57,
       "age": 24,
       "height": 186,
       "role": "MID"
    },
    "lachlan weller": {
       "first_name": "Lachlan",
       "last_name": "Weller",
       "current_team": "GCS",
       "number": 14,
       "games": 136,
       "age": 28,
       "height": 182,
       "role": "DEF"
    },
    "laitham vandermeer": {
       "first_name": "Laitham",
       "last_name": "Vandermeer",
       "current_team": "WBD",
       "number": 23,
       "games": 50,
       "age": 25,
       "height": 182,
       "role": "DEF FWD"
    },
    "lance collard": {
       "first_name": "Lance",
       "last_name": "Collard",
       "current_team": "STK",
       "number": 4,
       "games": 1,
       "age": 19,
       "height": 185,
       "role": "FWD"
    },
    "leek aleer": {
       "first_name": "Leek",
       "last_name": "Aleer",
       "current_team": "GWS",
       "number": 21,
       "games": 6,
       "age": 22,
       "height": 194,
       "role": "DEF"
    },
    "levi casboult": {
       "first_name": "Levi",
       "last_name": "Casboult",
       "current_team": "GCS",
       "number": 30,
       "games": 198,
       "age": 34,
       "height": 199,
       "role": "FWD"
    },
    "lewis melican": {
       "first_name": "Lewis",
       "last_name": "Melican",
       "current_team": "SYD",
       "number": 43,
       "games": 66,
       "age": 27,
       "height": 194,
       "role": "DEF"
    },
    "lewis young": {
       "first_name": "Lewis",
       "last_name": "Young",
       "current_team": "CAR",
       "number": 33,
       "games": 61,
       "age": 25,
       "height": 202,
       "role": "DEF"
    },
    "liam baker": {
       "first_name": "Liam",
       "last_name": "Baker",
       "current_team": "RIC",
       "number": 7,
       "games": 116,
       "age": 26,
       "height": 173,
       "role": "DEF FWD"
    },
    "liam duggan": {
       "first_name": "Liam",
       "last_name": "Duggan",
       "current_team": "WCE",
       "number": 14,
       "games": 164,
       "age": 27,
       "height": 186,
       "role": "DEF"
    },
    "liam henry": {
       "first_name": "Liam",
       "last_name": "Henry",
       "current_team": "STK",
       "number": 23,
       "games": 45,
       "age": 22,
       "height": 180,
       "role": "MID"
    },
    "liam jones": {
       "first_name": "Liam",
       "last_name": "Jones",
       "current_team": "WBD",
       "number": 19,
       "games": 185,
       "age": 33,
       "height": 199,
       "role": "DEF"
    },
    "liam ryan": {
       "first_name": "Liam",
       "last_name": "Ryan",
       "current_team": "WCE",
       "number": 1,
       "games": 89,
       "age": 27,
       "height": 181,
       "role": "FWD"
    },
    "liam shiels": {
       "first_name": "Liam",
       "last_name": "Shiels",
       "current_team": "NOR",
       "number": 14,
       "games": 273,
       "age": 32,
       "height": 184,
       "role": "MID"
    },
    "liam stocker": {
       "first_name": "Liam",
       "last_name": "Stocker",
       "current_team": "STK",
       "number": 14,
       "games": 57,
       "age": 24,
       "height": 184,
       "role": "DEF"
    },
    "lincoln mccarthy": {
       "first_name": "Lincoln",
       "last_name": "McCarthy",
       "current_team": "BRI",
       "number": 11,
       "games": 150,
       "age": 30,
       "height": 178,
       "role": "FWD"
    },
    "lloyd johnston": {
       "first_name": "Lloyd",
       "last_name": "Johnston",
       "current_team": "GCS",
       "number": 38,
       "games": 2,
       "age": 19,
       "height": 184,
       "role": "DEF"
    },
    "lloyd meek": {
       "first_name": "Lloyd",
       "last_name": "Meek",
       "current_team": "HAW",
       "number": 17,
       "games": 35,
       "age": 26,
       "height": 203,
       "role": "RCK"
    },
    "loch rawlinson": {
       "first_name": "Loch",
       "last_name": "Rawlinson",
       "current_team": "WCE",
       "number": 36,
       "games": 1,
       "age": 18,
       "height": 178,
       "role": "MID FWD"
    },
    "logan mcdonald": {
       "first_name": "Logan",
       "last_name": "McDonald",
       "current_team": "SYD",
       "number": 6,
       "games": 50,
       "age": 22,
       "height": 195,
       "role": "FWD"
    },
    "luke breust": {
       "first_name": "Luke",
       "last_name": "Breust",
       "current_team": "HAW",
       "number": 22,
       "games": 284,
       "age": 33,
       "height": 182,
       "role": "FWD"
    },
    "luke cleary": {
       "first_name": "Luke",
       "last_name": "Cleary",
       "current_team": "WBD",
       "number": 36,
       "games": 5,
       "age": 22,
       "height": 191,
       "role": "DEF"
    },
    "luke davies-uniacke": {
       "first_name": "Luke",
       "last_name": "Davies-Uniacke",
       "current_team": "NOR",
       "number": 9,
       "games": 91,
       "age": 24,
       "height": 188,
       "role": "MID"
    },
    "luke edwards": {
       "first_name": "Luke",
       "last_name": "Edwards",
       "current_team": "WCE",
       "number": 16,
       "games": 30,
       "age": 22,
       "height": 189,
       "role": "MID FWD"
    },
    "luke jackson": {
       "first_name": "Luke",
       "last_name": "Jackson",
       "current_team": "FRE",
       "number": 9,
       "games": 81,
       "age": 22,
       "height": 199,
       "role": "FWD RCK"
    },
    "luke mcdonald": {
       "first_name": "Luke",
       "last_name": "McDonald",
       "current_team": "NOR",
       "number": 11,
       "games": 186,
       "age": 29,
       "height": 189,
       "role": "DEF"
    },
    "luke nankervis": {
       "first_name": "Luke",
       "last_name": "Nankervis",
       "current_team": "ADE",
       "number": 27,
       "games": 7,
       "age": 20,
       "height": 191,
       "role": "DEF MID"
    },
    "luke parker": {
       "first_name": "Luke",
       "last_name": "Parker",
       "current_team": "SYD",
       "number": 26,
       "games": 283,
       "age": 31,
       "height": 183,
       "role": "MID"
    },
    "luke pedlar": {
       "first_name": "Luke",
       "last_name": "Pedlar",
       "current_team": "ADE",
       "number": 10,
       "games": 29,
       "age": 21,
       "height": 183,
       "role": "FWD"
    },
    "luke ryan": {
       "first_name": "Luke",
       "last_name": "Ryan",
       "current_team": "FRE",
       "number": 13,
       "games": 138,
       "age": 28,
       "height": 186,
       "role": "DEF"
    },
    "mabior chol": {
       "first_name": "Mabior",
       "last_name": "Chol",
       "current_team": "HAW",
       "number": 18,
       "games": 67,
       "age": 27,
       "height": 200,
       "role": "FWD"
    },
    "mac andrew": {
       "first_name": "Mac",
       "last_name": "Andrew",
       "current_team": "GCS",
       "number": 31,
       "games": 24,
       "age": 20,
       "height": 202,
       "role": "DEF"
    },
    "malcolm rosas": {
       "first_name": "Malcolm",
       "last_name": "Rosas",
       "current_team": "GCS",
       "number": 41,
       "games": 42,
       "age": 22,
       "height": 180,
       "role": "FWD"
    },
    "marc pittonet": {
       "first_name": "Marc",
       "last_name": "Pittonet",
       "current_team": "CAR",
       "number": 27,
       "games": 61,
       "age": 27,
       "height": 202,
       "role": "RCK"
    },
    "marcus bontempelli": {
       "first_name": "Marcus",
       "last_name": "Bontempelli",
       "current_team": "WBD",
       "number": 4,
       "games": 222,
       "age": 28,
       "height": 194,
       "role": "MID"
    },
    "marcus windhager": {
       "first_name": "Marcus",
       "last_name": "Windhager",
       "current_team": "STK",
       "number": 2,
       "games": 42,
       "age": 20,
       "height": 185,
       "role": "DEF MID"
    },
    "mark blicavs": {
       "first_name": "Mark",
       "last_name": "Blicavs",
       "current_team": "GEE",
       "number": 46,
       "games": 252,
       "age": 33,
       "height": 198,
       "role": "MID RCK"
    },
    "mark keane": {
       "first_name": "Mark",
       "last_name": "Keane",
       "current_team": "ADE",
       "number": 48,
       "games": 15,
       "age": 24,
       "height": 194,
       "role": "DEF"
    },
    "mark o'connor": {
       "first_name": "Mark",
       "last_name": "O'Connor",
       "current_team": "GEE",
       "number": 42,
       "games": 115,
       "age": 27,
       "height": 189,
       "role": "DEF MID"
    },
    "marlion pickett": {
       "first_name": "Marlion",
       "last_name": "Pickett",
       "current_team": "RIC",
       "number": 50,
       "games": 85,
       "age": 32,
       "height": 184,
       "role": "MID FWD"
    },
    "marty hore": {
       "first_name": "Marty",
       "last_name": "Hore",
       "current_team": "MEL",
       "number": 27,
       "games": 17,
       "age": 28,
       "height": 190,
       "role": "DEF"
    },
    "mason cox": {
       "first_name": "Mason",
       "last_name": "Cox",
       "current_team": "COL",
       "number": 46,
       "games": 120,
       "age": 33,
       "height": 211,
       "role": "FWD RCK"
    },
    "mason redman": {
       "first_name": "Mason",
       "last_name": "Redman",
       "current_team": "ESS",
       "number": 27,
       "games": 109,
       "age": 26,
       "height": 187,
       "role": "DEF"
    },
    "mason wood": {
       "first_name": "Mason",
       "last_name": "Wood",
       "current_team": "STK",
       "number": 32,
       "games": 119,
       "age": 30,
       "height": 192,
       "role": "MID"
    },
    "massimo d'ambrosio": {
       "first_name": "Massimo",
       "last_name": "D'Ambrosio",
       "current_team": "HAW",
       "number": 16,
       "games": 22,
       "age": 20,
       "height": 178,
       "role": "DEF MID"
    },
    "matt cottrell": {
       "first_name": "Matt",
       "last_name": "Cottrell",
       "current_team": "CAR",
       "number": 46,
       "games": 60,
       "age": 24,
       "height": 184,
       "role": "MID FWD"
    },
    "matt coulthard": {
       "first_name": "Matt",
       "last_name": "Coulthard",
       "current_team": "RIC",
       "number": 26,
       "games": 4,
       "age": 22,
       "height": 176,
       "role": "FWD"
    },
    "matt crouch": {
       "first_name": "Matt",
       "last_name": "Crouch",
       "current_team": "ADE",
       "number": 5,
       "games": 148,
       "age": 29,
       "height": 183,
       "role": "MID"
    },
    "matt guelfi": {
       "first_name": "Matt",
       "last_name": "Guelfi",
       "current_team": "ESS",
       "number": 35,
       "games": 97,
       "age": 26,
       "height": 184,
       "role": "FWD"
    },
    "matt johnson": {
       "first_name": "Matt",
       "last_name": "Johnson",
       "current_team": "FRE",
       "number": 44,
       "games": 24,
       "age": 21,
       "height": 193,
       "role": "MID"
    },
    "matt owies": {
       "first_name": "Matt",
       "last_name": "Owies",
       "current_team": "CAR",
       "number": 44,
       "games": 55,
       "age": 27,
       "height": 179,
       "role": "FWD"
    },
    "matt rowell": {
       "first_name": "Matt",
       "last_name": "Rowell",
       "current_team": "GCS",
       "number": 18,
       "games": 68,
       "age": 22,
       "height": 180,
       "role": "MID"
    },
    "mattaes phillipou": {
       "first_name": "Mattaes",
       "last_name": "Phillipou",
       "current_team": "STK",
       "number": 25,
       "games": 30,
       "age": 19,
       "height": 190,
       "role": "FWD"
    },
    "matthew flynn": {
       "first_name": "Matthew",
       "last_name": "Flynn",
       "current_team": "WCE",
       "number": 25,
       "games": 33,
       "age": 26,
       "height": 202,
       "role": "RCK"
    },
    "matthew kennedy": {
       "first_name": "Matthew",
       "last_name": "Kennedy",
       "current_team": "CAR",
       "number": 7,
       "games": 100,
       "age": 27,
       "height": 188,
       "role": "MID FWD"
    },
    "matthew roberts": {
       "first_name": "Matthew",
       "last_name": "Roberts",
       "current_team": "SYD",
       "number": 34,
       "games": 13,
       "age": 20,
       "height": 184,
       "role": "DEF MID"
    },
    "matthew taberner": {
       "first_name": "Matthew",
       "last_name": "Taberner",
       "current_team": "FRE",
       "number": 20,
       "games": 124,
       "age": 30,
       "height": 198,
       "role": "FWD"
    },
    "maurice rioli": {
       "first_name": "Maurice",
       "last_name": "Rioli",
       "current_team": "RIC",
       "number": 10,
       "games": 33,
       "age": 21,
       "height": 179,
       "role": "FWD"
    },
    "max gawn": {
       "first_name": "Max",
       "last_name": "Gawn",
       "current_team": "MEL",
       "number": 11,
       "games": 210,
       "age": 32,
       "height": 209,
       "role": "RCK"
    },
    "max holmes": {
       "first_name": "Max",
       "last_name": "Holmes",
       "current_team": "GEE",
       "number": 9,
       "games": 57,
       "age": 21,
       "height": 190,
       "role": "DEF MID"
    },
    "max king": {
       "first_name": "Max",
       "last_name": "King",
       "current_team": "STK",
       "number": 12,
       "games": 75,
       "age": 23,
       "height": 202,
       "role": "FWD"
    },
    "max michalanney": {
       "first_name": "Max",
       "last_name": "Michalanney",
       "current_team": "ADE",
       "number": 16,
       "games": 28,
       "age": 20,
       "height": 190,
       "role": "DEF"
    },
    "max ramsden": {
       "first_name": "Max",
       "last_name": "Ramsden",
       "current_team": "HAW",
       "number": 38,
       "games": 4,
       "age": 21,
       "height": 203,
       "role": "FWD"
    },
    "michael frederick": {
       "first_name": "Michael",
       "last_name": "Frederick",
       "current_team": "FRE",
       "number": 32,
       "games": 62,
       "age": 23,
       "height": 183,
       "role": "FWD"
    },
    "michael walters": {
       "first_name": "Michael",
       "last_name": "Walters",
       "current_team": "FRE",
       "number": 10,
       "games": 228,
       "age": 33,
       "height": 176,
       "role": "FWD"
    },
    "miles bergman": {
       "first_name": "Miles",
       "last_name": "Bergman",
       "current_team": "POR",
       "number": 14,
       "games": 63,
       "age": 22,
       "height": 189,
       "role": "DEF MID"
    },
    "miller bergman": {
       "first_name": "Miller",
       "last_name": "Bergman",
       "current_team": "NOR",
       "number": 27,
       "games": 13,
       "age": 21,
       "height": 189,
       "role": "DEF"
    },
    "mitch georgiades": {
       "first_name": "Mitch",
       "last_name": "Georgiades",
       "current_team": "POR",
       "number": 19,
       "games": 52,
       "age": 22,
       "height": 192,
       "role": "FWD"
    },
    "mitch mcgovern": {
       "first_name": "Mitch",
       "last_name": "McGovern",
       "current_team": "CAR",
       "number": 11,
       "games": 115,
       "age": 29,
       "height": 190,
       "role": "DEF"
    },
    "mitchell duncan": {
       "first_name": "Mitchell",
       "last_name": "Duncan",
       "current_team": "GEE",
       "number": 22,
       "games": 279,
       "age": 32,
       "height": 188,
       "role": "DEF"
    },
    "mitchell hinge": {
       "first_name": "Mitchell",
       "last_name": "Hinge",
       "current_team": "ADE",
       "number": 20,
       "games": 49,
       "age": 25,
       "height": 190,
       "role": "DEF"
    },
    "mitchell knevitt": {
       "first_name": "Mitchell",
       "last_name": "Knevitt",
       "current_team": "GEE",
       "number": 10,
       "games": 11,
       "age": 21,
       "height": 193,
       "role": "MID"
    },
    "mitchell lewis": {
       "first_name": "Mitchell",
       "last_name": "Lewis",
       "current_team": "HAW",
       "number": 2,
       "games": 69,
       "age": 25,
       "height": 199,
       "role": "FWD"
    },
    "mitchito owens": {
       "first_name": "Mitchito",
       "last_name": "Owens",
       "current_team": "STK",
       "number": 10,
       "games": 36,
       "age": 20,
       "height": 191,
       "role": "FWD"
    },
    "mykelti lefau": {
       "first_name": "Mykelti",
       "last_name": "Lefau",
       "current_team": "RIC",
       "number": 42,
       "games": 5,
       "age": 25,
       "height": 194,
       "role": "FWD"
    },
    "nasiah wanganeen-milera": {
       "first_name": "Nasiah",
       "last_name": "Wanganeen-Milera",
       "current_team": "STK",
       "number": 7,
       "games": 47,
       "age": 21,
       "height": 187,
       "role": "DEF"
    },
    "nathan broad": {
       "first_name": "Nathan",
       "last_name": "Broad",
       "current_team": "RIC",
       "number": 35,
       "games": 132,
       "age": 31,
       "height": 192,
       "role": "DEF"
    },
    "nathan fyfe": {
       "first_name": "Nathan",
       "last_name": "Fyfe",
       "current_team": "FRE",
       "number": 7,
       "games": 224,
       "age": 32,
       "height": 191,
       "role": "MID FWD"
    },
    "nathan kreuger": {
       "first_name": "Nathan",
       "last_name": "Kreuger",
       "current_team": "COL",
       "number": 15,
       "games": 9,
       "age": 24,
       "height": 196,
       "role": "FWD RCK"
    },
    "nathan murphy": {
       "first_name": "Nathan",
       "last_name": "Murphy",
       "current_team": "COL",
       "number": 28,
       "games": 57,
       "age": 24,
       "height": 192,
       "role": "DEF"
    },
    "nathan o'driscoll": {
       "first_name": "Nathan",
       "last_name": "O'Driscoll",
       "current_team": "FRE",
       "number": 30,
       "games": 22,
       "age": 21,
       "height": 188,
       "role": "MID"
    },
    "ned mchenry": {
       "first_name": "Ned",
       "last_name": "McHenry",
       "current_team": "ADE",
       "number": 25,
       "games": 65,
       "age": 23,
       "height": 179,
       "role": "FWD"
    },
    "ned moyle": {
       "first_name": "Ned",
       "last_name": "Moyle",
       "current_team": "GCS",
       "number": 49,
       "games": 3,
       "age": 22,
       "height": 206,
       "role": "RCK"
    },
    "ned reeves": {
       "first_name": "Ned",
       "last_name": "Reeves",
       "current_team": "HAW",
       "number": 7,
       "games": 40,
       "age": 25,
       "height": 210,
       "role": "RCK"
    },
    "neil erasmus": {
       "first_name": "Neil",
       "last_name": "Erasmus",
       "current_team": "FRE",
       "number": 28,
       "games": 22,
       "age": 20,
       "height": 190,
       "role": "MID FWD"
    },
    "nic newman": {
       "first_name": "Nic",
       "last_name": "Newman",
       "current_team": "CAR",
       "number": 24,
       "games": 116,
       "age": 31,
       "height": 186,
       "role": "DEF"
    },
    "nicholas coffield": {
       "first_name": "Nicholas",
       "last_name": "Coffield",
       "current_team": "WBD",
       "number": 17,
       "games": 54,
       "age": 24,
       "height": 191,
       "role": "DEF"
    },
    "nicholas holman": {
       "first_name": "Nicholas",
       "last_name": "Holman",
       "current_team": "GCS",
       "number": 7,
       "games": 116,
       "age": 28,
       "height": 188,
       "role": "FWD"
    },
    "nicholas martin": {
       "first_name": "Nicholas",
       "last_name": "Martin",
       "current_team": "ESS",
       "number": 37,
       "games": 51,
       "age": 23,
       "height": 192,
       "role": "DEF MID"
    },
    "nick blakey": {
       "first_name": "Nick",
       "last_name": "Blakey",
       "current_team": "SYD",
       "number": 22,
       "games": 108,
       "age": 24,
       "height": 196,
       "role": "DEF"
    },
    "nick bryan": {
       "first_name": "Nick",
       "last_name": "Bryan",
       "current_team": "ESS",
       "number": 24,
       "games": 14,
       "age": 22,
       "height": 203,
       "role": "RCK"
    },
    "nick daicos": {
       "first_name": "Nick",
       "last_name": "Daicos",
       "current_team": "COL",
       "number": 35,
       "games": 54,
       "age": 21,
       "height": 184,
       "role": "DEF MID"
    },
    "nick haynes": {
       "first_name": "Nick",
       "last_name": "Haynes",
       "current_team": "GWS",
       "number": 19,
       "games": 205,
       "age": 31,
       "height": 192,
       "role": "DEF"
    },
    "nick hind": {
       "first_name": "Nick",
       "last_name": "Hind",
       "current_team": "ESS",
       "number": 19,
       "games": 86,
       "age": 29,
       "height": 180,
       "role": "DEF FWD"
    },
    "nick larkey": {
       "first_name": "Nick",
       "last_name": "Larkey",
       "current_team": "NOR",
       "number": 20,
       "games": 100,
       "age": 25,
       "height": 198,
       "role": "FWD"
    },
    "nick murray": {
       "first_name": "Nick",
       "last_name": "Murray",
       "current_team": "ADE",
       "number": 28,
       "games": 46,
       "age": 23,
       "height": 194,
       "role": "DEF"
    },
    "nick vlastuin": {
       "first_name": "Nick",
       "last_name": "Vlastuin",
       "current_team": "RIC",
       "number": 1,
       "games": 218,
       "age": 30,
       "height": 187,
       "role": "DEF"
    },
    "nick watson": {
       "first_name": "Nick",
       "last_name": "Watson",
       "current_team": "HAW",
       "number": 34,
       "games": 3,
       "age": 19,
       "height": 170,
       "role": "MID FWD"
    },
    "nikolas cox": {
       "first_name": "Nikolas",
       "last_name": "Cox",
       "current_team": "ESS",
       "number": 13,
       "games": 40,
       "age": 22,
       "height": 200,
       "role": "DEF MID"
    },
    "noah anderson": {
       "first_name": "Noah",
       "last_name": "Anderson",
       "current_team": "GCS",
       "number": 15,
       "games": 87,
       "age": 23,
       "height": 192,
       "role": "MID"
    },
    "noah answerth": {
       "first_name": "Noah",
       "last_name": "Answerth",
       "current_team": "BRI",
       "number": 43,
       "games": 61,
       "age": 24,
       "height": 183,
       "role": "DEF FWD"
    },
    "noah balta": {
       "first_name": "Noah",
       "last_name": "Balta",
       "current_team": "RIC",
       "number": 21,
       "games": 89,
       "age": 24,
       "height": 194,
       "role": "DEF FWD"
    },
    "noah cumberland": {
       "first_name": "Noah",
       "last_name": "Cumberland",
       "current_team": "RIC",
       "number": 38,
       "games": 18,
       "age": 23,
       "height": 183,
       "role": "FWD"
    },
    "noah long": {
       "first_name": "Noah",
       "last_name": "Long",
       "current_team": "WCE",
       "number": 13,
       "games": 25,
       "age": 19,
       "height": 179,
       "role": "FWD"
    },
    "oisin mullin": {
       "first_name": "Oisin",
       "last_name": "Mullin",
       "current_team": "GEE",
       "number": 34,
       "games": 8,
       "age": 24,
       "height": 182,
       "role": "DEF"
    },
    "oleg markov": {
       "first_name": "Oleg",
       "last_name": "Markov",
       "current_team": "COL",
       "number": 37,
       "games": 77,
       "age": 27,
       "height": 188,
       "role": "DEF"
    },
    "oliver florent": {
       "first_name": "Oliver",
       "last_name": "Florent",
       "current_team": "SYD",
       "number": 13,
       "games": 148,
       "age": 25,
       "height": 184,
       "role": "DEF"
    },
    "oliver henry": {
       "first_name": "Oliver",
       "last_name": "Henry",
       "current_team": "GEE",
       "number": 36,
       "games": 53,
       "age": 21,
       "height": 189,
       "role": "FWD"
    },
    "oliver hollands": {
       "first_name": "Oliver",
       "last_name": "Hollands",
       "current_team": "CAR",
       "number": 4,
       "games": 25,
       "age": 20,
       "height": 184,
       "role": "MID"
    },
    "oliver wines": {
       "first_name": "Oliver",
       "last_name": "Wines",
       "current_team": "POR",
       "number": 16,
       "games": 233,
       "age": 29,
       "height": 187,
       "role": "MID"
    },
    "ollie dempsey": {
       "first_name": "Ollie",
       "last_name": "Dempsey",
       "current_team": "GEE",
       "number": 28,
       "games": 13,
       "age": 21,
       "height": 187,
       "role": "MID FWD"
    },
    "ollie lord": {
       "first_name": "Ollie",
       "last_name": "Lord",
       "current_team": "POR",
       "number": 30,
       "games": 13,
       "age": 22,
       "height": 197,
       "role": "FWD"
    },
    "orazio fantasia": {
       "first_name": "Orazio",
       "last_name": "Fantasia",
       "current_team": "CAR",
       "number": 14,
       "games": 105,
       "age": 28,
       "height": 180,
       "role": "FWD"
    },
    "oscar allen": {
       "first_name": "Oscar",
       "last_name": "Allen",
       "current_team": "WCE",
       "number": 12,
       "games": 83,
       "age": 25,
       "height": 196,
       "role": "FWD"
    },
    "oscar mcdonald": {
       "first_name": "Oscar",
       "last_name": "McDonald",
       "current_team": "FRE",
       "number": 21,
       "games": 87,
       "age": 28,
       "height": 196,
       "role": "DEF"
    },
    "oscar mcinerney": {
       "first_name": "Oscar",
       "last_name": "McInerney",
       "current_team": "BRI",
       "number": 46,
       "games": 132,
       "age": 29,
       "height": 204,
       "role": "RCK"
    },
    "oskar baker": {
       "first_name": "Oskar",
       "last_name": "Baker",
       "current_team": "WBD",
       "number": 13,
       "games": 37,
       "age": 25,
       "height": 184,
       "role": "MID"
    },
    "paddy dow": {
       "first_name": "Paddy",
       "last_name": "Dow",
       "current_team": "STK",
       "number": 15,
       "games": 73,
       "age": 24,
       "height": 184,
       "role": "MID"
    },
    "patrick cripps": {
       "first_name": "Patrick",
       "last_name": "Cripps",
       "current_team": "CAR",
       "number": 9,
       "games": 189,
       "age": 29,
       "height": 195,
       "role": "MID"
    },
    "patrick dangerfield": {
       "first_name": "Patrick",
       "last_name": "Dangerfield",
       "current_team": "GEE",
       "number": 35,
       "games": 324,
       "age": 34,
       "height": 189,
       "role": "MID"
    },
    "patrick lipinski": {
       "first_name": "Patrick",
       "last_name": "Lipinski",
       "current_team": "COL",
       "number": 1,
       "games": 102,
       "age": 25,
       "height": 190,
       "role": "MID FWD"
    },
    "patrick parnell": {
       "first_name": "Patrick",
       "last_name": "Parnell",
       "current_team": "ADE",
       "number": 37,
       "games": 17,
       "age": 22,
       "height": 178,
       "role": "DEF"
    },
    "paul curtis": {
       "first_name": "Paul",
       "last_name": "Curtis",
       "current_team": "NOR",
       "number": 25,
       "games": 42,
       "age": 21,
       "height": 185,
       "role": "FWD"
    },
    "peter ladhams": {
       "first_name": "Peter",
       "last_name": "Ladhams",
       "current_team": "SYD",
       "number": 19,
       "games": 52,
       "age": 26,
       "height": 204,
       "role": "RCK"
    },
    "peter wright": {
       "first_name": "Peter",
       "last_name": "Wright",
       "current_team": "ESS",
       "number": 20,
       "games": 122,
       "age": 27,
       "height": 203,
       "role": "FWD"
    },
    "quinton narkle": {
       "first_name": "Quinton",
       "last_name": "Narkle",
       "current_team": "POR",
       "number": 42,
       "games": 44,
       "age": 26,
       "height": 182,
       "role": "FWD"
    },
    "reef mcinnes": {
       "first_name": "Reef",
       "last_name": "McInnes",
       "current_team": "COL",
       "number": 26,
       "games": 15,
       "age": 21,
       "height": 194,
       "role": "FWD"
    },
    "reilly o'brien": {
       "first_name": "Reilly",
       "last_name": "O'Brien",
       "current_team": "ADE",
       "number": 43,
       "games": 106,
       "age": 28,
       "height": 202,
       "role": "RCK"
    },
    "reuben ginbey": {
       "first_name": "Reuben",
       "last_name": "Ginbey",
       "current_team": "WCE",
       "number": 7,
       "games": 23,
       "age": 19,
       "height": 191,
       "role": "MID"
    },
    "rhett bazzo": {
       "first_name": "Rhett",
       "last_name": "Bazzo",
       "current_team": "WCE",
       "number": 33,
       "games": 20,
       "age": 20,
       "height": 195,
       "role": "DEF"
    },
    "rhyan mansell": {
       "first_name": "Rhyan",
       "last_name": "Mansell",
       "current_team": "RIC",
       "number": 31,
       "games": 38,
       "age": 23,
       "height": 180,
       "role": "FWD"
    },
    "rhylee west": {
       "first_name": "Rhylee",
       "last_name": "West",
       "current_team": "WBD",
       "number": 14,
       "games": 43,
       "age": 23,
       "height": 183,
       "role": "FWD"
    },
    "rhys stanley": {
       "first_name": "Rhys",
       "last_name": "Stanley",
       "current_team": "GEE",
       "number": 1,
       "games": 200,
       "age": 33,
       "height": 200,
       "role": "RCK"
    },
    "riley bonner": {
       "first_name": "Riley",
       "last_name": "Bonner",
       "current_team": "STK",
       "number": 36,
       "games": 99,
       "age": 27,
       "height": 190,
       "role": "DEF MID"
    },
    "riley garcia": {
       "first_name": "Riley",
       "last_name": "Garcia",
       "current_team": "WBD",
       "number": 38,
       "games": 20,
       "age": 23,
       "height": 178,
       "role": "FWD"
    },
    "riley thilthorpe": {
       "first_name": "Riley",
       "last_name": "Thilthorpe",
       "current_team": "ADE",
       "number": 7,
       "games": 46,
       "age": 21,
       "height": 202,
       "role": "FWD"
    },
    "robbie fox": {
       "first_name": "Robbie",
       "last_name": "Fox",
       "current_team": "SYD",
       "number": 42,
       "games": 92,
       "age": 31,
       "height": 185,
       "role": "DEF FWD"
    },
    "robert hansen": {
       "first_name": "Robert",
       "last_name": "Hansen",
       "current_team": "NOR",
       "number": 46,
       "games": 2,
       "age": 20,
       "height": 182,
       "role": "FWD"
    },
    "rory atkins": {
       "first_name": "Rory",
       "last_name": "Atkins",
       "current_team": "GCS",
       "number": 2,
       "games": 136,
       "age": 29,
       "height": 185,
       "role": "DEF"
    },
    "rory laird": {
       "first_name": "Rory",
       "last_name": "Laird",
       "current_team": "ADE",
       "number": 29,
       "games": 230,
       "age": 30,
       "height": 177,
       "role": "MID"
    },
    "rory lobb": {
       "first_name": "Rory",
       "last_name": "Lobb",
       "current_team": "WBD",
       "number": 7,
       "games": 162,
       "age": 31,
       "height": 207,
       "role": "FWD"
    },
    "rory sloane": {
       "first_name": "Rory",
       "last_name": "Sloane",
       "current_team": "ADE",
       "number": 9,
       "games": 255,
       "age": 34,
       "height": 183,
       "role": "MID"
    },
    "rowan marshall": {
       "first_name": "Rowan",
       "last_name": "Marshall",
       "current_team": "STK",
       "number": 19,
       "games": 116,
       "age": 28,
       "height": 201,
       "role": "RCK"
    },
    "ryan angwin": {
       "first_name": "Ryan",
       "last_name": "Angwin",
       "current_team": "GWS",
       "number": 9,
       "games": 16,
       "age": 21,
       "height": 185,
       "role": "MID"
    },
    "ryan burton": {
       "first_name": "Ryan",
       "last_name": "Burton",
       "current_team": "POR",
       "number": 3,
       "games": 145,
       "age": 27,
       "height": 191,
       "role": "DEF"
    },
    "ryan byrnes": {
       "first_name": "Ryan",
       "last_name": "Byrnes",
       "current_team": "STK",
       "number": 13,
       "games": 54,
       "age": 22,
       "height": 181,
       "role": "MID"
    },
    "ryan gardner": {
       "first_name": "Ryan",
       "last_name": "Gardner",
       "current_team": "WBD",
       "number": 43,
       "games": 53,
       "age": 26,
       "height": 197,
       "role": "DEF"
    },
    "ryan lester": {
       "first_name": "Ryan",
       "last_name": "Lester",
       "current_team": "BRI",
       "number": 35,
       "games": 187,
       "age": 31,
       "height": 192,
       "role": "DEF"
    },
    "ryan maric": {
       "first_name": "Ryan",
       "last_name": "Maric",
       "current_team": "WCE",
       "number": 41,
       "games": 15,
       "age": 19,
       "height": 196,
       "role": "FWD"
    },
    "ryley sanders": {
       "first_name": "Ryley",
       "last_name": "Sanders",
       "current_team": "WBD",
       "number": 9,
       "games": 6,
       "age": 19,
       "height": 186,
       "role": "MID FWD"
    },
    "sam banks": {
       "first_name": "Sam",
       "last_name": "Banks",
       "current_team": "RIC",
       "number": 41,
       "games": 10,
       "age": 21,
       "height": 187,
       "role": "DEF MID"
    },
    "sam berry": {
       "first_name": "Sam",
       "last_name": "Berry",
       "current_team": "ADE",
       "number": 3,
       "games": 44,
       "age": 22,
       "height": 182,
       "role": "MID"
    },
    "sam butler": {
       "first_name": "Sam",
       "last_name": "Butler",
       "current_team": "HAW",
       "number": 30,
       "games": 19,
       "age": 21,
       "height": 184,
       "role": "FWD"
    },
    "sam clohesy": {
       "first_name": "Sam",
       "last_name": "Clohesy",
       "current_team": "GCS",
       "number": 33,
       "games": 3,
       "age": 21,
       "height": 189,
       "role": "DEF MID"
    },
    "sam darcy": {
       "first_name": "Sam",
       "last_name": "Darcy",
       "current_team": "WBD",
       "number": 10,
       "games": 12,
       "age": 20,
       "height": 208,
       "role": "FWD"
    },
    "sam day": {
       "first_name": "Sam",
       "last_name": "Day",
       "current_team": "GCS",
       "number": 12,
       "games": 150,
       "age": 31,
       "height": 196,
       "role": "FWD"
    },
    "sam de koning": {
       "first_name": "Sam",
       "last_name": "De Koning",
       "current_team": "GEE",
       "number": 16,
       "games": 49,
       "age": 23,
       "height": 204,
       "role": "DEF"
    },
    "sam docherty": {
       "first_name": "Sam",
       "last_name": "Docherty",
       "current_team": "CAR",
       "number": 15,
       "games": 168,
       "age": 30,
       "height": 187,
       "role": "MID"
    },
    "sam draper": {
       "first_name": "Sam",
       "last_name": "Draper",
       "current_team": "ESS",
       "number": 2,
       "games": 63,
       "age": 25,
       "height": 205,
       "role": "FWD RCK"
    },
    "sam durdin": {
       "first_name": "Sam",
       "last_name": "Durdin",
       "current_team": "CAR",
       "number": 38,
       "games": 23,
       "age": 27,
       "height": 199,
       "role": "DEF"
    },
    "sam durham": {
       "first_name": "Sam",
       "last_name": "Durham",
       "current_team": "ESS",
       "number": 22,
       "games": 55,
       "age": 22,
       "height": 185,
       "role": "MID"
    },
    "sam flanders": {
       "first_name": "Sam",
       "last_name": "Flanders",
       "current_team": "GCS",
       "number": 3,
       "games": 50,
       "age": 22,
       "height": 183,
       "role": "MID FWD"
    },
    "sam frost": {
       "first_name": "Sam",
       "last_name": "Frost",
       "current_team": "HAW",
       "number": 8,
       "games": 165,
       "age": 30,
       "height": 194,
       "role": "DEF"
    },
    "sam naismith": {
       "first_name": "Sam",
       "last_name": "Naismith",
       "current_team": "RIC",
       "number": 24,
       "games": 32,
       "age": 31,
       "height": 205,
       "role": "RCK"
    },
    "sam powell-pepper": {
       "first_name": "Sam",
       "last_name": "Powell-Pepper",
       "current_team": "POR",
       "number": 2,
       "games": 141,
       "age": 26,
       "height": 187,
       "role": "FWD"
    },
    "sam reid": {
       "first_name": "Sam",
       "last_name": "Reid",
       "current_team": "SYD",
       "number": 20,
       "games": 181,
       "age": 32,
       "height": 196,
       "role": "FWD"
    },
    "sam sturt": {
       "first_name": "Sam",
       "last_name": "Sturt",
       "current_team": "FRE",
       "number": 1,
       "games": 19,
       "age": 23,
       "height": 190,
       "role": "FWD"
    },
    "sam switkowski": {
       "first_name": "Sam",
       "last_name": "Switkowski",
       "current_team": "FRE",
       "number": 39,
       "games": 73,
       "age": 27,
       "height": 179,
       "role": "FWD"
    },
    "sam taylor": {
       "first_name": "Sam",
       "last_name": "Taylor",
       "current_team": "GWS",
       "number": 15,
       "games": 98,
       "age": 24,
       "height": 198,
       "role": "DEF"
    },
    "sam walsh": {
       "first_name": "Sam",
       "last_name": "Walsh",
       "current_team": "CAR",
       "number": 18,
       "games": 101,
       "age": 23,
       "height": 184,
       "role": "MID"
    },
    "sam weideman": {
       "first_name": "Sam",
       "last_name": "Weideman",
       "current_team": "ESS",
       "number": 10,
       "games": 75,
       "age": 26,
       "height": 197,
       "role": "FWD"
    },
    "sam wicks": {
       "first_name": "Sam",
       "last_name": "Wicks",
       "current_team": "SYD",
       "number": 15,
       "games": 54,
       "age": 24,
       "height": 181,
       "role": "FWD"
    },
    "samson ryan": {
       "first_name": "Samson",
       "last_name": "Ryan",
       "current_team": "RIC",
       "number": 32,
       "games": 16,
       "age": 23,
       "height": 206,
       "role": "FWD RCK"
    },
    "samuel collins": {
       "first_name": "Samuel",
       "last_name": "Collins",
       "current_team": "GCS",
       "number": 25,
       "games": 110,
       "age": 29,
       "height": 194,
       "role": "DEF"
    },
    "scott pendlebury": {
       "first_name": "Scott",
       "last_name": "Pendlebury",
       "current_team": "COL",
       "number": 10,
       "games": 390,
       "age": 36,
       "height": 191,
       "role": "MID"
    },
    "seamus mitchell": {
       "first_name": "Seamus",
       "last_name": "Mitchell",
       "current_team": "HAW",
       "number": 40,
       "games": 17,
       "age": 21,
       "height": 181,
       "role": "DEF"
    },
    "sean darcy": {
       "first_name": "Sean",
       "last_name": "Darcy",
       "current_team": "FRE",
       "number": 4,
       "games": 99,
       "age": 25,
       "height": 203,
       "role": "RCK"
    },
    "sean lemmens": {
       "first_name": "Sean",
       "last_name": "Lemmens",
       "current_team": "GCS",
       "number": 23,
       "games": 145,
       "age": 29,
       "height": 184,
       "role": "DEF"
    },
    "sebastian ross": {
       "first_name": "Sebastian",
       "last_name": "Ross",
       "current_team": "STK",
       "number": 6,
       "games": 204,
       "age": 30,
       "height": 187,
       "role": "MID"
    },
    "seth campbell": {
       "first_name": "Seth",
       "last_name": "Campbell",
       "current_team": "RIC",
       "number": 44,
       "games": 7,
       "age": 19,
       "height": 182,
       "role": "FWD"
    },
    "shai bolton": {
       "first_name": "Shai",
       "last_name": "Bolton",
       "current_team": "RIC",
       "number": 29,
       "games": 120,
       "age": 25,
       "height": 176,
       "role": "MID FWD"
    },
    "shane mcadam": {
       "first_name": "Shane",
       "last_name": "McAdam",
       "current_team": "MEL",
       "number": 23,
       "games": 50,
       "age": 28,
       "height": 186,
       "role": "FWD"
    },
    "shannon neale": {
       "first_name": "Shannon",
       "last_name": "Neale",
       "current_team": "GEE",
       "number": 33,
       "games": 6,
       "age": 21,
       "height": 203,
       "role": "FWD"
    },
    "shaun mannagh": {
       "first_name": "Shaun",
       "last_name": "Mannagh",
       "current_team": "GEE",
       "number": 7,
       "games": 2,
       "age": 26,
       "height": 178,
       "role": "MID FWD"
    },
    "steele sidebottom": {
       "first_name": "Steele",
       "last_name": "Sidebottom",
       "current_team": "COL",
       "number": 22,
       "games": 315,
       "age": 33,
       "height": 184,
       "role": "MID"
    },
    "stephen coniglio": {
       "first_name": "Stephen",
       "last_name": "Coniglio",
       "current_team": "GWS",
       "number": 3,
       "games": 207,
       "age": 30,
       "height": 182,
       "role": "MID"
    },
    "steven may": {
       "first_name": "Steven",
       "last_name": "May",
       "current_team": "MEL",
       "number": 1,
       "games": 222,
       "age": 32,
       "height": 193,
       "role": "DEF"
    },
    "taj woewodin": {
       "first_name": "Taj",
       "last_name": "Woewodin",
       "current_team": "MEL",
       "number": 40,
       "games": 9,
       "age": 21,
       "height": 182,
       "role": "DEF FWD"
    },
    "tanner bruhn": {
       "first_name": "Tanner",
       "last_name": "Bruhn",
       "current_team": "GEE",
       "number": 4,
       "games": 54,
       "age": 21,
       "height": 184,
       "role": "MID"
    },
    "tarryn thomas": {
       "first_name": "Tarryn",
       "last_name": "Thomas",
       "current_team": "NOR",
       "number": 26,
       "games": 69,
       "age": 24,
       "height": 190,
       "role": "MID"
    },
    "taylor adams": {
       "first_name": "Taylor",
       "last_name": "Adams",
       "current_team": "SYD",
       "number": 3,
       "games": 208,
       "age": 30,
       "height": 181,
       "role": "MID FWD"
    },
    "taylor duryea": {
       "first_name": "Taylor",
       "last_name": "Duryea",
       "current_team": "WBD",
       "number": 15,
       "games": 196,
       "age": 33,
       "height": 181,
       "role": "DEF"
    },
    "taylor walker": {
       "first_name": "Taylor",
       "last_name": "Walker",
       "current_team": "ADE",
       "number": 13,
       "games": 265,
       "age": 34,
       "height": 194,
       "role": "FWD"
    },
    "ted clohesy": {
       "first_name": "Ted",
       "last_name": "Clohesy",
       "current_team": "GEE",
       "number": 40,
       "games": 1,
       "age": 19,
       "height": 184,
       "role": "FWD"
    },
    "tex wanganeen": {
       "first_name": "Tex",
       "last_name": "Wanganeen",
       "current_team": "ESS",
       "number": 40,
       "games": 5,
       "age": 20,
       "height": 179,
       "role": "FWD"
    },
    "thomas berry": {
       "first_name": "Thomas",
       "last_name": "Berry",
       "current_team": "GCS",
       "number": 16,
       "games": 32,
       "age": 23,
       "height": 185,
       "role": "FWD"
    },
    "thomas liberatore": {
       "first_name": "Thomas",
       "last_name": "Liberatore",
       "current_team": "WBD",
       "number": 21,
       "games": 221,
       "age": 31,
       "height": 184,
       "role": "MID"
    },
    "thomas stewart": {
       "first_name": "Thomas",
       "last_name": "Stewart",
       "current_team": "GEE",
       "number": 44,
       "games": 154,
       "age": 31,
       "height": 190,
       "role": "DEF"
    },
    "thomson dow": {
       "first_name": "Thomson",
       "last_name": "Dow",
       "current_team": "RIC",
       "number": 27,
       "games": 24,
       "age": 22,
       "height": 184,
       "role": "MID"
    },
    "tim kelly": {
       "first_name": "Tim",
       "last_name": "Kelly",
       "current_team": "WCE",
       "number": 11,
       "games": 130,
       "age": 29,
       "height": 184,
       "role": "MID"
    },
    "tim membrey": {
       "first_name": "Tim",
       "last_name": "Membrey",
       "current_team": "STK",
       "number": 28,
       "games": 166,
       "age": 29,
       "height": 188,
       "role": "FWD"
    },
    "tim taranto": {
       "first_name": "Tim",
       "last_name": "Taranto",
       "current_team": "RIC",
       "number": 14,
       "games": 142,
       "age": 26,
       "height": 188,
       "role": "MID"
    },
    "timothy english": {
       "first_name": "Timothy",
       "last_name": "English",
       "current_team": "WBD",
       "number": 44,
       "games": 114,
       "age": 26,
       "height": 208,
       "role": "RCK"
    },
    "toby bedford": {
       "first_name": "Toby",
       "last_name": "Bedford",
       "current_team": "GWS",
       "number": 14,
       "games": 44,
       "age": 23,
       "height": 179,
       "role": "FWD"
    },
    "toby conway": {
       "first_name": "Toby",
       "last_name": "Conway",
       "current_team": "GEE",
       "number": 6,
       "games": 3,
       "age": 21,
       "height": 206,
       "role": "RCK"
    },
    "toby greene": {
       "first_name": "Toby",
       "last_name": "Greene",
       "current_team": "GWS",
       "number": 4,
       "games": 221,
       "age": 30,
       "height": 182,
       "role": "FWD"
    },
    "toby mcmullin": {
       "first_name": "Toby",
       "last_name": "McMullin",
       "current_team": "GWS",
       "number": 31,
       "games": 4,
       "age": 19,
       "height": 183,
       "role": "FWD"
    },
    "toby nankervis": {
       "first_name": "Toby",
       "last_name": "Nankervis",
       "current_team": "RIC",
       "number": 25,
       "games": 142,
       "age": 29,
       "height": 199,
       "role": "RCK"
    },
    "toby pink": {
       "first_name": "Toby",
       "last_name": "Pink",
       "current_team": "NOR",
       "number": 32,
       "games": 3,
       "age": 25,
       "height": 194,
       "role": "DEF"
    },
    "todd goldstein": {
       "first_name": "Todd",
       "last_name": "Goldstein",
       "current_team": "ESS",
       "number": 17,
       "games": 322,
       "age": 35,
       "height": 201,
       "role": "RCK"
    },
    "todd marshall": {
       "first_name": "Todd",
       "last_name": "Marshall",
       "current_team": "POR",
       "number": 4,
       "games": 103,
       "age": 25,
       "height": 198,
       "role": "FWD"
    },
    "tom atkins": {
       "first_name": "Tom",
       "last_name": "Atkins",
       "current_team": "GEE",
       "number": 30,
       "games": 110,
       "age": 28,
       "height": 180,
       "role": "DEF MID"
    },
    "tom barrass": {
       "first_name": "Tom",
       "last_name": "Barrass",
       "current_team": "WCE",
       "number": 37,
       "games": 138,
       "age": 28,
       "height": 197,
       "role": "DEF"
    },
    "tom brown": {
       "first_name": "Tom",
       "last_name": "Brown",
       "current_team": "RIC",
       "number": 30,
       "games": 6,
       "age": 20,
       "height": 193,
       "role": "DEF"
    },
    "tom campbell": {
       "first_name": "Tom",
       "last_name": "Campbell",
       "current_team": "STK",
       "number": 38,
       "games": 56,
       "age": 32,
       "height": 201,
       "role": "RCK"
    },
    "tom clurey": {
       "first_name": "Tom",
       "last_name": "Clurey",
       "current_team": "POR",
       "number": 17,
       "games": 124,
       "age": 30,
       "height": 193,
       "role": "DEF"
    },
    "tom cole": {
       "first_name": "Tom",
       "last_name": "Cole",
       "current_team": "WCE",
       "number": 28,
       "games": 96,
       "age": 26,
       "height": 188,
       "role": "DEF"
    },
    "tom de koning": {
       "first_name": "Tom",
       "last_name": "De Koning",
       "current_team": "CAR",
       "number": 12,
       "games": 66,
       "age": 24,
       "height": 201,
       "role": "RCK"
    },
    "tom doedee": {
       "first_name": "Tom",
       "last_name": "Doedee",
       "current_team": "BRI",
       "number": 12,
       "games": 82,
       "age": 27,
       "height": 190,
       "role": "DEF"
    },
    "tom emmett": {
       "first_name": "Tom",
       "last_name": "Emmett",
       "current_team": "FRE",
       "number": 18,
       "games": 8,
       "age": 22,
       "height": 186,
       "role": "FWD"
    },
    "tom fullarton": {
       "first_name": "Tom",
       "last_name": "Fullarton",
       "current_team": "MEL",
       "number": 33,
       "games": 19,
       "age": 25,
       "height": 200,
       "role": "FWD"
    },
    "tom green": {
       "first_name": "Tom",
       "last_name": "Green",
       "current_team": "GWS",
       "number": 12,
       "games": 74,
       "age": 23,
       "height": 192,
       "role": "MID"
    },
    "tom hawkins": {
       "first_name": "Tom",
       "last_name": "Hawkins",
       "current_team": "GEE",
       "number": 26,
       "games": 352,
       "age": 35,
       "height": 197,
       "role": "FWD"
    },
    "tom lynch": {
       "first_name": "Tom",
       "last_name": "Lynch",
       "current_team": "RIC",
       "number": 19,
       "games": 219,
       "age": 31,
       "height": 199,
       "role": "FWD"
    },
    "tom mccartin": {
       "first_name": "Tom",
       "last_name": "McCartin",
       "current_team": "SYD",
       "number": 30,
       "games": 116,
       "age": 24,
       "height": 194,
       "role": "DEF"
    },
    "tom mcdonald": {
       "first_name": "Tom",
       "last_name": "McDonald",
       "current_team": "MEL",
       "number": 25,
       "games": 216,
       "age": 31,
       "height": 195,
       "role": "DEF FWD"
    },
    "tom mitchell": {
       "first_name": "Tom",
       "last_name": "Mitchell",
       "current_team": "COL",
       "number": 6,
       "games": 203,
       "age": 30,
       "height": 182,
       "role": "MID"
    },
    "tom papley": {
       "first_name": "Tom",
       "last_name": "Papley",
       "current_team": "SYD",
       "number": 11,
       "games": 170,
       "age": 27,
       "height": 177,
       "role": "FWD"
    },
    "tom powell": {
       "first_name": "Tom",
       "last_name": "Powell",
       "current_team": "NOR",
       "number": 24,
       "games": 51,
       "age": 22,
       "height": 184,
       "role": "MID FWD"
    },
    "tom sparrow": {
       "first_name": "Tom",
       "last_name": "Sparrow",
       "current_team": "MEL",
       "number": 32,
       "games": 81,
       "age": 23,
       "height": 183,
       "role": "MID FWD"
    },
    "touk miller": {
       "first_name": "Touk",
       "last_name": "Miller",
       "current_team": "GCS",
       "number": 11,
       "games": 179,
       "age": 28,
       "height": 178,
       "role": "MID"
    },
    "travis boak": {
       "first_name": "Travis",
       "last_name": "Boak",
       "current_team": "POR",
       "number": 10,
       "games": 352,
       "age": 35,
       "height": 183,
       "role": "MID"
    },
    "trent mckenzie": {
       "first_name": "Trent",
       "last_name": "McKenzie",
       "current_team": "POR",
       "number": 12,
       "games": 165,
       "age": 32,
       "height": 191,
       "role": "DEF"
    },
    "trent rivers": {
       "first_name": "Trent",
       "last_name": "Rivers",
       "current_team": "MEL",
       "number": 24,
       "games": 84,
       "age": 22,
       "height": 188,
       "role": "DEF"
    },
    "tristan xerri": {
       "first_name": "Tristan",
       "last_name": "Xerri",
       "current_team": "NOR",
       "number": 38,
       "games": 39,
       "age": 25,
       "height": 201,
       "role": "RCK"
    },
    "tylar young": {
       "first_name": "Tylar",
       "last_name": "Young",
       "current_team": "RIC",
       "number": 45,
       "games": 25,
       "age": 25,
       "height": 196,
       "role": "DEF"
    },
    "tyler brockman": {
       "first_name": "Tyler",
       "last_name": "Brockman",
       "current_team": "WCE",
       "number": 10,
       "games": 31,
       "age": 21,
       "height": 180,
       "role": "FWD"
    },
    "tyler sellers": {
       "first_name": "Tyler",
       "last_name": "Sellers",
       "current_team": "NOR",
       "number": 36,
       "games": 2,
       "age": 21,
       "height": 193,
       "role": "FWD"
    },
    "tyler sonsie": {
       "first_name": "Tyler",
       "last_name": "Sonsie",
       "current_team": "RIC",
       "number": 40,
       "games": 13,
       "age": 21,
       "height": 181,
       "role": "FWD"
    },
    "tyson stengle": {
       "first_name": "Tyson",
       "last_name": "Stengle",
       "current_team": "GEE",
       "number": 18,
       "games": 66,
       "age": 25,
       "height": 175,
       "role": "FWD"
    },
    "wayne milera": {
       "first_name": "Wayne",
       "last_name": "Milera",
       "current_team": "ADE",
       "number": 30,
       "games": 99,
       "age": 26,
       "height": 184,
       "role": "DEF"
    },
    "wil powell": {
       "first_name": "Wil",
       "last_name": "Powell",
       "current_team": "GCS",
       "number": 27,
       "games": 93,
       "age": 24,
       "height": 188,
       "role": "DEF"
    },
    "will ashcroft": {
       "first_name": "Will",
       "last_name": "Ashcroft",
       "current_team": "BRI",
       "number": 8,
       "games": 18,
       "age": 19,
       "height": 182,
       "role": "MID"
    },
    "will brodie": {
       "first_name": "Will",
       "last_name": "Brodie",
       "current_team": "FRE",
       "number": 17,
       "games": 54,
       "age": 25,
       "height": 190,
       "role": "MID"
    },
    "will day": {
       "first_name": "Will",
       "last_name": "Day",
       "current_team": "HAW",
       "number": 12,
       "games": 54,
       "age": 22,
       "height": 191,
       "role": "MID"
    },
    "will graham": {
       "first_name": "Will",
       "last_name": "Graham",
       "current_team": "GCS",
       "number": 26,
       "games": 3,
       "age": 18,
       "height": 186,
       "role": "DEF MID"
    },
    "will hamill": {
       "first_name": "Will",
       "last_name": "Hamill",
       "current_team": "ADE",
       "number": 17,
       "games": 40,
       "age": 23,
       "height": 187,
       "role": "DEF"
    },
    "will hayward": {
       "first_name": "Will",
       "last_name": "Hayward",
       "current_team": "SYD",
       "number": 9,
       "games": 143,
       "age": 25,
       "height": 186,
       "role": "FWD"
    },
    "will hoskin-elliott": {
       "first_name": "Will",
       "last_name": "Hoskin-Elliott",
       "current_team": "COL",
       "number": 32,
       "games": 210,
       "age": 30,
       "height": 186,
       "role": "DEF MID"
    },
    "will phillips": {
       "first_name": "Will",
       "last_name": "Phillips",
       "current_team": "NOR",
       "number": 29,
       "games": 33,
       "age": 21,
       "height": 181,
       "role": "MID"
    },
    "will setterfield": {
       "first_name": "Will",
       "last_name": "Setterfield",
       "current_team": "ESS",
       "number": 12,
       "games": 70,
       "age": 26,
       "height": 192,
       "role": "MID"
    },
    "willem drew": {
       "first_name": "Willem",
       "last_name": "Drew",
       "current_team": "POR",
       "number": 28,
       "games": 87,
       "age": 25,
       "height": 188,
       "role": "MID"
    },
    "willie rioli": {
       "first_name": "Willie",
       "last_name": "Rioli",
       "current_team": "POR",
       "number": 15,
       "games": 76,
       "age": 28,
       "height": 175,
       "role": "FWD"
    },
    "xavier duursma": {
       "first_name": "Xavier",
       "last_name": "Duursma",
       "current_team": "ESS",
       "number": 28,
       "games": 80,
       "age": 23,
       "height": 186,
       "role": "MID"
    },
    "xavier o'halloran": {
       "first_name": "Xavier",
       "last_name": "O'Halloran",
       "current_team": "GWS",
       "number": 33,
       "games": 55,
       "age": 23,
       "height": 187,
       "role": "MID FWD"
    },
    "zac bailey": {
       "first_name": "Zac",
       "last_name": "Bailey",
       "current_team": "BRI",
       "number": 33,
       "games": 123,
       "age": 24,
       "height": 182,
       "role": "MID FWD"
    },
    "zac fisher": {
       "first_name": "Zac",
       "last_name": "Fisher",
       "current_team": "NOR",
       "number": 16,
       "games": 113,
       "age": 25,
       "height": 175,
       "role": "DEF FWD"
    },
    "zach guthrie": {
       "first_name": "Zach",
       "last_name": "Guthrie",
       "current_team": "GEE",
       "number": 39,
       "games": 83,
       "age": 25,
       "height": 188,
       "role": "DEF"
    },
    "zach reid": {
       "first_name": "Zach",
       "last_name": "Reid",
       "current_team": "ESS",
       "number": 31,
       "games": 9,
       "age": 22,
       "height": 202,
       "role": "DEF"
    },
    "zach tuohy": {
       "first_name": "Zach",
       "last_name": "Tuohy",
       "current_team": "GEE",
       "number": 2,
       "games": 274,
       "age": 34,
       "height": 187,
       "role": "MID"
    },
    "zachary merrett": {
       "first_name": "Zachary",
       "last_name": "Merrett",
       "current_team": "ESS",
       "number": 7,
       "games": 213,
       "age": 28,
       "height": 179,
       "role": "MID"
    },
    "zachary williams": {
       "first_name": "Zachary",
       "last_name": "Williams",
       "current_team": "CAR",
       "number": 6,
       "games": 142,
       "age": 29,
       "height": 186,
       "role": "DEF"
    },
    "zaine cordy": {
       "first_name": "Zaine",
       "last_name": "Cordy",
       "current_team": "STK",
       "number": 21,
       "games": 138,
       "age": 27,
       "height": 195,
       "role": "DEF FWD"
    },
    "zak butters": {
       "first_name": "Zak",
       "last_name": "Butters",
       "current_team": "POR",
       "number": 9,
       "games": 99,
       "age": 23,
       "height": 181,
       "role": "MID"
    },
    "zak jones": {
       "first_name": "Zak",
       "last_name": "Jones",
       "current_team": "STK",
       "number": 3,
       "games": 140,
       "age": 29,
       "height": 182,
       "role": "MID"
    },
    "zane duursma": {
       "first_name": "Zane",
       "last_name": "Duursma",
       "current_team": "NOR",
       "number": 7,
       "games": 6,
       "age": 18,
       "height": 190,
       "role": "MID FWD"
    },
    "zane trew": {
       "first_name": "Zane",
       "last_name": "Trew",
       "current_team": "WCE",
       "number": 26,
       "games": 6,
       "age": 21,
       "height": 187,
       "role": "MID"
    }
};

var prompt = require('prompt-sync')({
    autocomplete: complete(["aaron cadman", "aaron francis", "aaron naughton", "adam cerra", "adam kennedy", "adam saad", "adam tomlinson", "adam treloar", "aidan corr", "aiden begg", "alex cincotta", "alex davies", "alex keath", "alex neal-bullen", "alex pearce", "alex sexton", "alex witherden", "aliir aliir", "alwyn davey", "andrew brayshaw", "andrew gaff", "andrew mcgrath", "angus brayshaw", "angus hastie", "angus sheldrick", "anthony caminiti", "anthony scott", "archie perkins", "arthur jones", "ash johnson", "bailey banfield", "bailey dale", "bailey humphrey", "bailey j. williams", "bailey laurie", "bailey macdonald", "bailey scott", "bailey smith", "bailey williams", "bayley fritsch", "beau mccreery", "ben ainsworth", "ben brown", "ben hobbs", "ben keays", "ben king", "ben long", "ben mckay", "ben miller", "ben paton", "bigoa nyuon", "billy frampton", "blake acres", "blake drury", "blake hardwick", "blake howes", "bobby hill", "bodhi uwland", "brad crouch", "bradley close", "bradley hill", "brady hough", "braeden campbell", "brandan parfitt", "brandon ellis", "brandon ryan", "brandon starcevich", "brandon walker", "brandon zerk-thatcher", "brayden cook", "brayden fiorini", "brayden maynard", "braydon preuss", "brennan cox", "brent daniels", "brodie grundy", "brodie kemp", "brodie smith", "brody mihocek", "buku khamis", "caleb daniel", "caleb graham", "caleb marchbank", "caleb mitchell", "caleb poulter", "caleb serong", "caleb windsor", "callan ward", "callum ah chee", "callum brown", "callum coleman-jones", "callum jamieson", "callum mills", "callum wilkie", "cameron guthrie", "cameron mackenzie", "cameron rayner", "cameron zurhaar", "campbell chesser", "carter michael", "chad warner", "chad wingard", "changkuoth jiath", "charlie ballard", "charlie cameron", "charlie comben", "charlie curnow", "charlie dean", "charlie dixon", "charlie lazzaro", "charlie spargo", "chayce jones", "chris burgess", "christian petracca", "christian salem", "clayton oliver", "cody weightman", "colby mckercher", "connor budarick", "connor idun", "connor macdonald", "connor o'sullivan", "connor rozee", "conor mckenna", "conor nash", "conor stone", "cooper hamilton", "cooper harvey", "cooper sharman", "cooper stephens", "corey durdin", "corey wagner", "corey warner", "curtis taylor", "dan houston", "dane rampe", "daniel butler", "daniel mcstay", "daniel rioli", "daniel turner", "dante visentini", "darcy byrne-jones", "darcy cameron", "darcy fogarty", "darcy fort", "darcy gardiner", "darcy jones", "darcy macpherson", "darcy moore", "darcy parish", "darcy tucker", "darcy wilmot", "darcy wilson", "darragh joyce", "david cuningham", "david swallow", "dayne zorko", "denver grainger-barras", "deven robertson", "dion prestia", "dominic bedendo", "dominic sheed", "dougal howard", "dustin martin", "dylan grimes", "dylan moore", "dylan shiel", "dylan stephens", "dylan williams", "dyson heppell", "ed langdon", "ed richards", "eddie ford", "elijah hewett", "elijah hollands", "elijah tsatas", "elliot yeo", "elliott himmelberg", "emerson jeka", "eric hipwood", "errol gulden", "esava ratugolea", "ethan hughes", "ethan read", "ethan stanley", "finlay macrae", "finn callaghan", "finn maginness", "francis evans", "gary rohan", "george hewett", "george wardlaw", "griffin logue", "gryan miers", "harley reid", "harris andrews", "harrison himmelberg", "harrison jones", "harrison petty", "harry barnett", "harry cunningham", "harry edwards", "harry mckay", "harry morrison", "harry perryman", "harry rowston", "harry schoenberg", "harry sharp", "harry sheezel", "harvey gallagher", "harvey harrison", "harvey thomas", "hayden mclean", "hayden young", "heath chapman", "henry hustwaite", "hewago oea", "hugh greenwood", "hugh mccluggage", "hugo garcia", "hugo ralphsmith", "hunter clark", "isaac cumming", "isaac heeney", "isaac quaynor", "ivan soldo", "izak rankine", "jack billings", "jack bowes", "jack buckley", "jack buller", "jack bytel", "jack carroll", "jack crisp", "jack darling", "jack ginnivan", "jack graham", "jack gunston", "jack hayes", "jack henry", "jack higgins", "jack lukosius", "jack mahony", "jack martin", "jack payne", "jack petruccelle", "jack ross", "jack scrimshaw", "jack silvagni", "jack sinclair", "jack steele", "jack viney", "jack williams", "jackson archer", "jackson macrae", "jackson mead", "jacob bauer", "jacob hopper", "jacob koschitzke", "jacob van rooyen", "jacob wehr", "jacob weitering", "jade gresham", "jaeger o'meara", "jai culley", "jai newcombe", "jai serong", "jaidyn stephenson", "jake bowey", "jake kelly", "jake kolodjashnij", "jake lever", "jake lloyd", "jake melksham", "jake riccardi", "jake rogers", "jake soligo", "jake stringer", "jake waterman", "jakob ryan", "jamaine jones", "jamarra ugle-hagan", "james aish", "james blanck", "james borlase", "james harmes", "james jordon", "james madden", "james o'donnell", "james peatling", "james rowbottom", "james sicily", "james trezise", "james tsitas", "james tunstill", "james worpel", "jamie cripps", "jamie elliott", "jarman impey", "jarrod berry", "jarrod witts", "jarryd lyons", "jase burgoyne", "jason horne-francis", "jason johannisen", "jaspa fletcher", "jaxon prior", "jayden hunt", "jayden laverde", "jayden short", "jed bews", "jed mcentee", "jed walter", "jeremy cameron", "jeremy finlayson", "jeremy howe", "jeremy mcgovern", "jeremy sharp", "jesse hogan", "jesse motlop", "jhye clark", "jimmy webster", "joe daniher", "joel amartey", "joel hamling", "joel jeffrey", "joel smith", "john noble", "jordan boyd", "jordan clark", "jordan dawson", "jordan de goey", "jordan ridley", "jordon butts", "jordon sweet", "josh battle", "josh carmichael", "josh corbett", "josh daicos", "josh draper", "josh dunkley", "josh fahey", "josh gibcus", "josh goater", "josh rachele", "josh rotham", "josh schache", "josh sinn", "josh treacy", "josh ward", "josh worrell", "joshua kelly", "joshua weddle", "judd mcvee", "judson clarke", "justin mcinerney", "jy farrar", "jy simpkin", "jye amiss", "jye caldwell", "jye menzie", "kade chandler", "kai lohmann", "kaine baldwin", "kallan dawson", "kamdyn mcintosh", "kane farrell", "kane mcauliffe", "karl amon", "karl worner", "keidean coleman", "kieran strachan", "kieren briggs", "koltyn tholstrup", "kyle langford", "kysaiah pickett", "lachie neale", "lachie whitfield", "lachlan ash", "lachlan bramble", "lachlan cowan", "lachlan fogarty", "lachlan gollant", "lachlan hunter", "lachlan jones", "lachlan keeffe", "lachlan mcandrew", "lachlan mcneil", "lachlan murphy", "lachlan schultz", "lachlan sholl", "lachlan weller", "laitham vandermeer", "lance collard", "leek aleer", "levi casboult", "lewis melican", "lewis young", "liam baker", "liam duggan", "liam henry", "liam jones", "liam ryan", "liam shiels", "liam stocker", "lincoln mccarthy", "lloyd johnston", "lloyd meek", "loch rawlinson", "logan mcdonald", "luke breust", "luke cleary", "luke davies-uniacke", "luke edwards", "luke jackson", "luke mcdonald", "luke nankervis", "luke parker", "luke pedlar", "luke ryan", "mabior chol", "mac andrew", "malcolm rosas", "marc pittonet", "marcus bontempelli", "marcus windhager", "mark blicavs", "mark keane", "mark o'connor", "marlion pickett", "marty hore", "mason cox", "mason redman", "mason wood", "massimo d'ambrosio", "matt cottrell", "matt coulthard", "matt crouch", "matt guelfi", "matt johnson", "matt owies", "matt rowell", "mattaes phillipou", "matthew flynn", "matthew kennedy", "matthew roberts", "matthew taberner", "maurice rioli", "max gawn", "max holmes", "max king", "max michalanney", "max ramsden", "michael frederick", "michael walters", "miles bergman", "miller bergman", "mitch georgiades", "mitch mcgovern", "mitchell duncan", "mitchell hinge", "mitchell knevitt", "mitchell lewis", "mitchito owens", "mykelti lefau", "nasiah wanganeen-milera", "nathan broad", "nathan fyfe", "nathan kreuger", "nathan murphy", "nathan o'driscoll", "ned mchenry", "ned moyle", "ned reeves", "neil erasmus", "nic newman", "nicholas coffield", "nicholas holman", "nicholas martin", "nick blakey", "nick bryan", "nick daicos", "nick haynes", "nick hind", "nick larkey", "nick murray", "nick vlastuin", "nick watson", "nikolas cox", "noah anderson", "noah answerth", "noah balta", "noah cumberland", "noah long", "oisin mullin", "oleg markov", "oliver florent", "oliver henry", "oliver hollands", "oliver wines", "ollie dempsey", "ollie lord", "orazio fantasia", "oscar allen", "oscar mcdonald", "oscar mcinerney", "oskar baker", "paddy dow", "patrick cripps", "patrick dangerfield", "patrick lipinski", "patrick parnell", "paul curtis", "peter ladhams", "peter wright", "quinton narkle", "reef mcinnes", "reilly o'brien", "reuben ginbey", "rhett bazzo", "rhyan mansell", "rhylee west", "rhys stanley", "riley bonner", "riley garcia", "riley thilthorpe", "robbie fox", "robert hansen", "rory atkins", "rory laird", "rory lobb", "rory sloane", "rowan marshall", "ryan angwin", "ryan burton", "ryan byrnes", "ryan gardner", "ryan lester", "ryan maric", "ryley sanders", "sam banks", "sam berry", "sam butler", "sam clohesy", "sam darcy", "sam day", "sam de koning", "sam docherty", "sam draper", "sam durdin", "sam durham", "sam flanders", "sam frost", "sam naismith", "sam powell-pepper", "sam reid", "sam sturt", "sam switkowski", "sam taylor", "sam walsh", "sam weideman", "sam wicks", "samson ryan", "samuel collins", "scott pendlebury", "seamus mitchell", "sean darcy", "sean lemmens", "sebastian ross", "seth campbell", "shai bolton", "shane mcadam", "shannon neale", "shaun mannagh", "steele sidebottom", "stephen coniglio", "steven may", "taj woewodin", "tanner bruhn", "tarryn thomas", "taylor adams", "taylor duryea", "taylor walker", "ted clohesy", "tex wanganeen", "thomas berry", "thomas liberatore", "thomas stewart", "thomson dow", "tim kelly", "tim membrey", "tim taranto", "timothy english", "toby bedford", "toby conway", "toby greene", "toby mcmullin", "toby nankervis", "toby pink", "todd goldstein", "todd marshall", "tom atkins", "tom barrass", "tom brown", "tom campbell", "tom clurey", "tom cole", "tom de koning", "tom doedee", "tom emmett", "tom fullarton", "tom green", "tom hawkins", "tom lynch", "tom mccartin", "tom mcdonald", "tom mitchell", "tom papley", "tom powell", "tom sparrow", "touk miller", "travis boak", "trent mckenzie", "trent rivers", "tristan xerri", "tylar young", "tyler brockman", "tyler sellers", "tyler sonsie", "tyson stengle", "wayne milera", "wil powell", "will ashcroft", "will brodie", "will day", "will graham", "will hamill", "will hayward", "will hoskin-elliott", "will phillips", "will setterfield", "willem drew", "willie rioli", "xavier duursma", "xavier o'halloran", "zac bailey", "zac fisher", "zach guthrie", "zach reid", "zach tuohy", "zachary merrett", "zachary williams", "zaine cordy", "zak butters", "zak jones", "zane duursma", "zane trew"]),
});

function complete(commands) {
    return function (str) {
      var i;
      var ret = [];
      for (i=0; i< commands.length; i++) {
        if (commands[i].indexOf(str) == 0)
          ret.push(commands[i]);
      }
      return ret;
    };
};

function request_input() {
    var user_input = prompt('Enter a player: ', {
        autocomplete: complete(["aaron cadman", "aaron francis", "aaron naughton", "adam cerra", "adam kennedy", "adam saad", "adam tomlinson", "adam treloar", "aidan corr", "aiden begg", "alex cincotta", "alex davies", "alex keath", "alex neal-bullen", "alex pearce", "alex sexton", "alex witherden", "aliir aliir", "alwyn davey", "andrew brayshaw", "andrew gaff", "andrew mcgrath", "angus brayshaw", "angus hastie", "angus sheldrick", "anthony caminiti", "anthony scott", "archie perkins", "arthur jones", "ash johnson", "bailey banfield", "bailey dale", "bailey humphrey", "bailey j. williams", "bailey laurie", "bailey macdonald", "bailey scott", "bailey smith", "bailey williams", "bayley fritsch", "beau mccreery", "ben ainsworth", "ben brown", "ben hobbs", "ben keays", "ben king", "ben long", "ben mckay", "ben miller", "ben paton", "bigoa nyuon", "billy frampton", "blake acres", "blake drury", "blake hardwick", "blake howes", "bobby hill", "bodhi uwland", "brad crouch", "bradley close", "bradley hill", "brady hough", "braeden campbell", "brandan parfitt", "brandon ellis", "brandon ryan", "brandon starcevich", "brandon walker", "brandon zerk-thatcher", "brayden cook", "brayden fiorini", "brayden maynard", "braydon preuss", "brennan cox", "brent daniels", "brodie grundy", "brodie kemp", "brodie smith", "brody mihocek", "buku khamis", "caleb daniel", "caleb graham", "caleb marchbank", "caleb mitchell", "caleb poulter", "caleb serong", "caleb windsor", "callan ward", "callum ah chee", "callum brown", "callum coleman-jones", "callum jamieson", "callum mills", "callum wilkie", "cameron guthrie", "cameron mackenzie", "cameron rayner", "cameron zurhaar", "campbell chesser", "carter michael", "chad warner", "chad wingard", "changkuoth jiath", "charlie ballard", "charlie cameron", "charlie comben", "charlie curnow", "charlie dean", "charlie dixon", "charlie lazzaro", "charlie spargo", "chayce jones", "chris burgess", "christian petracca", "christian salem", "clayton oliver", "cody weightman", "colby mckercher", "connor budarick", "connor idun", "connor macdonald", "connor o'sullivan", "connor rozee", "conor mckenna", "conor nash", "conor stone", "cooper hamilton", "cooper harvey", "cooper sharman", "cooper stephens", "corey durdin", "corey wagner", "corey warner", "curtis taylor", "dan houston", "dane rampe", "daniel butler", "daniel mcstay", "daniel rioli", "daniel turner", "dante visentini", "darcy byrne-jones", "darcy cameron", "darcy fogarty", "darcy fort", "darcy gardiner", "darcy jones", "darcy macpherson", "darcy moore", "darcy parish", "darcy tucker", "darcy wilmot", "darcy wilson", "darragh joyce", "david cuningham", "david swallow", "dayne zorko", "denver grainger-barras", "deven robertson", "dion prestia", "dominic bedendo", "dominic sheed", "dougal howard", "dustin martin", "dylan grimes", "dylan moore", "dylan shiel", "dylan stephens", "dylan williams", "dyson heppell", "ed langdon", "ed richards", "eddie ford", "elijah hewett", "elijah hollands", "elijah tsatas", "elliot yeo", "elliott himmelberg", "emerson jeka", "eric hipwood", "errol gulden", "esava ratugolea", "ethan hughes", "ethan read", "ethan stanley", "finlay macrae", "finn callaghan", "finn maginness", "francis evans", "gary rohan", "george hewett", "george wardlaw", "griffin logue", "gryan miers", "harley reid", "harris andrews", "harrison himmelberg", "harrison jones", "harrison petty", "harry barnett", "harry cunningham", "harry edwards", "harry mckay", "harry morrison", "harry perryman", "harry rowston", "harry schoenberg", "harry sharp", "harry sheezel", "harvey gallagher", "harvey harrison", "harvey thomas", "hayden mclean", "hayden young", "heath chapman", "henry hustwaite", "hewago oea", "hugh greenwood", "hugh mccluggage", "hugo garcia", "hugo ralphsmith", "hunter clark", "isaac cumming", "isaac heeney", "isaac quaynor", "ivan soldo", "izak rankine", "jack billings", "jack bowes", "jack buckley", "jack buller", "jack bytel", "jack carroll", "jack crisp", "jack darling", "jack ginnivan", "jack graham", "jack gunston", "jack hayes", "jack henry", "jack higgins", "jack lukosius", "jack mahony", "jack martin", "jack payne", "jack petruccelle", "jack ross", "jack scrimshaw", "jack silvagni", "jack sinclair", "jack steele", "jack viney", "jack williams", "jackson archer", "jackson macrae", "jackson mead", "jacob bauer", "jacob hopper", "jacob koschitzke", "jacob van rooyen", "jacob wehr", "jacob weitering", "jade gresham", "jaeger o'meara", "jai culley", "jai newcombe", "jai serong", "jaidyn stephenson", "jake bowey", "jake kelly", "jake kolodjashnij", "jake lever", "jake lloyd", "jake melksham", "jake riccardi", "jake rogers", "jake soligo", "jake stringer", "jake waterman", "jakob ryan", "jamaine jones", "jamarra ugle-hagan", "james aish", "james blanck", "james borlase", "james harmes", "james jordon", "james madden", "james o'donnell", "james peatling", "james rowbottom", "james sicily", "james trezise", "james tsitas", "james tunstill", "james worpel", "jamie cripps", "jamie elliott", "jarman impey", "jarrod berry", "jarrod witts", "jarryd lyons", "jase burgoyne", "jason horne-francis", "jason johannisen", "jaspa fletcher", "jaxon prior", "jayden hunt", "jayden laverde", "jayden short", "jed bews", "jed mcentee", "jed walter", "jeremy cameron", "jeremy finlayson", "jeremy howe", "jeremy mcgovern", "jeremy sharp", "jesse hogan", "jesse motlop", "jhye clark", "jimmy webster", "joe daniher", "joel amartey", "joel hamling", "joel jeffrey", "joel smith", "john noble", "jordan boyd", "jordan clark", "jordan dawson", "jordan de goey", "jordan ridley", "jordon butts", "jordon sweet", "josh battle", "josh carmichael", "josh corbett", "josh daicos", "josh draper", "josh dunkley", "josh fahey", "josh gibcus", "josh goater", "josh rachele", "josh rotham", "josh schache", "josh sinn", "josh treacy", "josh ward", "josh worrell", "joshua kelly", "joshua weddle", "judd mcvee", "judson clarke", "justin mcinerney", "jy farrar", "jy simpkin", "jye amiss", "jye caldwell", "jye menzie", "kade chandler", "kai lohmann", "kaine baldwin", "kallan dawson", "kamdyn mcintosh", "kane farrell", "kane mcauliffe", "karl amon", "karl worner", "keidean coleman", "kieran strachan", "kieren briggs", "koltyn tholstrup", "kyle langford", "kysaiah pickett", "lachie neale", "lachie whitfield", "lachlan ash", "lachlan bramble", "lachlan cowan", "lachlan fogarty", "lachlan gollant", "lachlan hunter", "lachlan jones", "lachlan keeffe", "lachlan mcandrew", "lachlan mcneil", "lachlan murphy", "lachlan schultz", "lachlan sholl", "lachlan weller", "laitham vandermeer", "lance collard", "leek aleer", "levi casboult", "lewis melican", "lewis young", "liam baker", "liam duggan", "liam henry", "liam jones", "liam ryan", "liam shiels", "liam stocker", "lincoln mccarthy", "lloyd johnston", "lloyd meek", "loch rawlinson", "logan mcdonald", "luke breust", "luke cleary", "luke davies-uniacke", "luke edwards", "luke jackson", "luke mcdonald", "luke nankervis", "luke parker", "luke pedlar", "luke ryan", "mabior chol", "mac andrew", "malcolm rosas", "marc pittonet", "marcus bontempelli", "marcus windhager", "mark blicavs", "mark keane", "mark o'connor", "marlion pickett", "marty hore", "mason cox", "mason redman", "mason wood", "massimo d'ambrosio", "matt cottrell", "matt coulthard", "matt crouch", "matt guelfi", "matt johnson", "matt owies", "matt rowell", "mattaes phillipou", "matthew flynn", "matthew kennedy", "matthew roberts", "matthew taberner", "maurice rioli", "max gawn", "max holmes", "max king", "max michalanney", "max ramsden", "michael frederick", "michael walters", "miles bergman", "miller bergman", "mitch georgiades", "mitch mcgovern", "mitchell duncan", "mitchell hinge", "mitchell knevitt", "mitchell lewis", "mitchito owens", "mykelti lefau", "nasiah wanganeen-milera", "nathan broad", "nathan fyfe", "nathan kreuger", "nathan murphy", "nathan o'driscoll", "ned mchenry", "ned moyle", "ned reeves", "neil erasmus", "nic newman", "nicholas coffield", "nicholas holman", "nicholas martin", "nick blakey", "nick bryan", "nick daicos", "nick haynes", "nick hind", "nick larkey", "nick murray", "nick vlastuin", "nick watson", "nikolas cox", "noah anderson", "noah answerth", "noah balta", "noah cumberland", "noah long", "oisin mullin", "oleg markov", "oliver florent", "oliver henry", "oliver hollands", "oliver wines", "ollie dempsey", "ollie lord", "orazio fantasia", "oscar allen", "oscar mcdonald", "oscar mcinerney", "oskar baker", "paddy dow", "patrick cripps", "patrick dangerfield", "patrick lipinski", "patrick parnell", "paul curtis", "peter ladhams", "peter wright", "quinton narkle", "reef mcinnes", "reilly o'brien", "reuben ginbey", "rhett bazzo", "rhyan mansell", "rhylee west", "rhys stanley", "riley bonner", "riley garcia", "riley thilthorpe", "robbie fox", "robert hansen", "rory atkins", "rory laird", "rory lobb", "rory sloane", "rowan marshall", "ryan angwin", "ryan burton", "ryan byrnes", "ryan gardner", "ryan lester", "ryan maric", "ryley sanders", "sam banks", "sam berry", "sam butler", "sam clohesy", "sam darcy", "sam day", "sam de koning", "sam docherty", "sam draper", "sam durdin", "sam durham", "sam flanders", "sam frost", "sam naismith", "sam powell-pepper", "sam reid", "sam sturt", "sam switkowski", "sam taylor", "sam walsh", "sam weideman", "sam wicks", "samson ryan", "samuel collins", "scott pendlebury", "seamus mitchell", "sean darcy", "sean lemmens", "sebastian ross", "seth campbell", "shai bolton", "shane mcadam", "shannon neale", "shaun mannagh", "steele sidebottom", "stephen coniglio", "steven may", "taj woewodin", "tanner bruhn", "tarryn thomas", "taylor adams", "taylor duryea", "taylor walker", "ted clohesy", "tex wanganeen", "thomas berry", "thomas liberatore", "thomas stewart", "thomson dow", "tim kelly", "tim membrey", "tim taranto", "timothy english", "toby bedford", "toby conway", "toby greene", "toby mcmullin", "toby nankervis", "toby pink", "todd goldstein", "todd marshall", "tom atkins", "tom barrass", "tom brown", "tom campbell", "tom clurey", "tom cole", "tom de koning", "tom doedee", "tom emmett", "tom fullarton", "tom green", "tom hawkins", "tom lynch", "tom mccartin", "tom mcdonald", "tom mitchell", "tom papley", "tom powell", "tom sparrow", "touk miller", "travis boak", "trent mckenzie", "trent rivers", "tristan xerri", "tylar young", "tyler brockman", "tyler sellers", "tyler sonsie", "tyson stengle", "wayne milera", "wil powell", "will ashcroft", "will brodie", "will day", "will graham", "will hamill", "will hayward", "will hoskin-elliott", "will phillips", "will setterfield", "willem drew", "willie rioli", "xavier duursma", "xavier o'halloran", "zac bailey", "zac fisher", "zach guthrie", "zach reid", "zach tuohy", "zachary merrett", "zachary williams", "zaine cordy", "zak butters", "zak jones", "zane duursma", "zane trew"])
    });
    return user_input;
}

// Enter desired answer below, or undo comments for random selection
//let player_answer = player_list["charlie cameron"];
let player_answer = player_list[pickRandomProperty(player_list)];

while (player_answer.games < 50) {
   player_answer = player_list[pickRandomProperty(player_list)];
}

let answer = new Player(player_answer.first_name, player_answer.last_name, player_answer.current_team, player_answer.role, player_answer.games, player_answer.height, player_answer.age, player_answer.number);

function pickRandomProperty(obj) {
   var prop, len = 0, randomPos, pos = 0;
   for (prop in obj) {
       if (obj.hasOwnProperty(prop)) {
           len += 1;
       }
   }
   randomPos = Math.floor(Math.random() * len);

   for (prop in obj) {
       if (obj.hasOwnProperty(prop)) {
           if (pos === randomPos) {
               return prop;
           }
           pos += 1;
       }
   }       
}


make_guess();

function make_guess() {
    let guess = request_input();
    if (player_list[guess]) {
        check_guess(guess);
    } else if (guess == "exit") {
        return;
    } else if (guess == "answer") {
        console.log(answer);
    } else {
        console.log("Player entered incorrectly");
        make_guess();
    }
}

function check_guess(guess) {

    let player_guess = player_list[guess];
    
    let player = new Player(player_guess.first_name, player_guess.last_name, player_guess.current_team, player_guess.role, player_guess.games, player_guess.height, player_guess.age, player_guess.number);

    const found = teamColours[player.team].some(r=> teamColours[answer.team].includes(r))
    
    if (player.first_name === answer.first_name) {
        console.log('\x1b[32m%s\x1b[0m', "First name is correct: " + player.first_name);
    } else {
        console.log('\x1b[31m%s\x1b[0m', "First name is wrong: " + player.first_name);
    }
    
    if (player.last_name === answer.last_name) {
        console.log('\x1b[32m%s\x1b[0m', "Last name is correct: " + player.last_name);
    } else {
        console.log('\x1b[31m%s\x1b[0m', "Last name is wrong: " + player.last_name);
    }
    
    if (player.team === answer.team) {
        console.log('\x1b[32m%s\x1b[0m', "Team is correct: " + player.team);
    } else if (found === true) {
        console.log('\x1b[33m%s\x1b[0m', "Team shares at least 1 colour: " + player.team);
    } else {
        console.log('\x1b[31m%s\x1b[0m', "Team is wrong: " + player.team);
    }

    if (player.role === answer.role) {
        console.log('\x1b[32m%s\x1b[0m', "Position is correct: " + player.role);
    } else if (player.role.length === 3 && answer.role.length === 7) {
        if (player.role.substr(0,3) === answer.role.substr(0,3) || player.role.substr(0,3) === answer.role.substr(4,7)) {
            console.log('\x1b[33m%s\x1b[0m', "Position is close: " + player.role);
        } else {
         console.log('\x1b[31m%s\x1b[0m', "Position is wrong: " + player.role);
     }
    } else if (player.role.length === 7 && answer.role.length === 3) {
        if (player.role.substr(0,3) === answer.role.substr(0,3) || player.role.substr(4,7) === answer.role.substr(0,3)) {
            console.log('\x1b[33m%s\x1b[0m', "Position is close: " + player.role);
        } else {
            console.log('\x1b[31m%s\x1b[0m', "Position is wrong: " + player.role);
        }
    } else {
        console.log('\x1b[31m%s\x1b[0m', "Position is wrong: " + player.role);
    }
    
    if (player.games === answer.games) {
        console.log('\x1b[32m%s\x1b[0m', "Games played is correct: " + player.games);
    } else if ((player.games - answer.games) < 51 && (player.games - answer.games) > 0) {
        console.log('\x1b[33m%s\x1b[0m', "Games played is 1-50 lower: " + player.games);
    } else if ((player.games - answer.games) > -51 && (player.games - answer.games) < 0) {
        console.log('\x1b[33m%s\x1b[0m', "Games played is 1-50 higher: " + player.games);
    } else if ((player.games - answer.games) > 50) {
        console.log('\x1b[31m%s\x1b[0m', "Games played is wrong (lower): " + player.games);
    } else if ((player.games - answer.games) < -50) {
        console.log('\x1b[31m%s\x1b[0m', "Games played is wrong (higher): " + player.games);
    }  else {
        console.log('\x1b[31m%s\x1b[0m', "Games played is wrong: " + player.games);
    }
    
    // Fix height bug, not showing amount incorrect by anymore
    if (player.height === answer.height) {
        console.log('\x1b[32m%s\x1b[0m', "Height is correct: " + player.height);
    } else if ((player.height - answer.height) < 4 && (player.height - answer.height) > 0) {
        console.log('\x1b[33m%s\x1b[0m', "Height is 1-3cm lower: " + player.height);
    } else if ((player.height - answer.height) > -4 && (player.height - answer.height) < 0) {
        console.log('\x1b[33m%s\x1b[0m', "Height is 1-3cm higher: " + player.height);
    } else if ((player.height - answer.height) > 3) {
        console.log('\x1b[31m%s\x1b[0m', "Height is wrong (lower): " + player.height);
    } else if ((player.height - answer.height) < -3) {
      console.log('\x1b[31m%s\x1b[0m', "Height is wrong (higher): " + player.height);
    } else {
        console.log('\x1b[31m%s\x1b[0m', "Height is wrong: " + player.height);
    }
    
    if (player.age === answer.age) {
        console.log('\x1b[32m%s\x1b[0m', "Age is correct: " + player.age);
    } else if ((player.age - answer.age) === 1) {
        console.log('\x1b[33m%s\x1b[0m', "Age is 1 year lower: " + player.age);
    } else if ((player.age - answer.age) === -1) {
        console.log('\x1b[33m%s\x1b[0m', "Age is 1 year higher: " + player.age);
    } else if ((player.age - answer.age) > 1) {
        console.log('\x1b[31m%s\x1b[0m', "Age is wrong (lower): " + player.age);
    } else if ((player.age - answer.age) < -1) {
        console.log('\x1b[31m%s\x1b[0m', "Age is wrong (higher): " + player.age);
    } else {
        console.log('\x1b[31m%s\x1b[0m', "Age is wrong: " + player.age);
    }
    
    if (player.number === answer.number) {
        console.log('\x1b[32m%s\x1b[0m', "Jumper number is correct: " + player.number);
    } else if ((player.number - answer.number) < 4 && (player.number - answer.number) > 0) {
        console.log('\x1b[33m%s\x1b[0m', "Jumper number is 1-3 lower: " + player.number);
    } else if ((player.number - answer.number) > -4 && (player.number - answer.number) < 0) {
        console.log('\x1b[33m%s\x1b[0m', "Jumper number is 1-3 higher: " + player.number);
    } else if ((player.number - answer.number) > 3) {
        console.log('\x1b[31m%s\x1b[0m', "Jumper number is wrong (lower): " + player.number);
    } else if ((player.number - answer.number) < -3) {
        console.log('\x1b[31m%s\x1b[0m', "Jumper number is wrong (higher): " + player.number);
    }  else {
        console.log('\x1b[31m%s\x1b[0m', "Jumper number is wrong: " + player.number);
    }
    
    if (player.first_name === answer.first_name && player.last_name === answer.last_name) {
        console.log("");
        console.log('\x1b[32m%s\x1b[0m', "YOU WIN!!!");
    } else {
        make_guess();
    }
}

