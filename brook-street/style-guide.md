# Front-end Style Guide

## Layout

The designs were created to the following widths:

- Desktop: 1920px

## Colors

### Header

- Background (Very Dark Blue): #272940
- Text (Grey): #B9B7B0

#### Body

- Background (Blue Gradient): linear-gradient(#1F256A to #272940)
- Icons (Dark Green): #1C806C
- Icon text (Green): #7C9A63
- Button (Dark Gold): #6A591F
- Button text (Gold): #B48E0C

## Typography

### Header

- Font size (Overview Card Headings): 60px

### Sub-Head

- Font size (Overview Card Headings): 40px

### Font

- Family: [Helvetica Neue]
- Weights: bold
