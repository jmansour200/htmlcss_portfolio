// add any icons that need buttons in the icons variable
let icons = document.querySelectorAll(".passwords .icon-button, .energy .icon-button, .internet .icon-button, .mygov .icon-button, .meal-delivery .icon-button");
// used for if comparators in event listener (e.g. could add bottom-button to this)
let buttonToggle = ["left-button", "middle-button", "right-button"];

// loop to call click listener function to create event listener (looking for click) for each icon that needs to show buttons
for (var i = 0; i < icons.length; i++) {
    clickListener(icons[i]);
}

function clickListener(item) {
    // getChild variable stores the HTML for the relevant article HTML, e.g. for the .passwords article element
    var getChilds = document.getElementsByClassName(item.id)[0];
    item.addEventListener("click", function(e) {
        // loop through children (each div under the article, e.g. icon-button, left-button, right-button)
        for (var i = 0; i < getChilds.childNodes.length; i++) {
            if (getChilds.childNodes[i].className) {
                // checks if child has one of the button classes, if it does then it will toggle the hide-button class
                if (getChilds.childNodes[i].className.substring(0,11) === buttonToggle[0] || getChilds.childNodes[i].className.substring(0,13) === buttonToggle[1] || getChilds.childNodes[i].className.substring(0,12) === buttonToggle[2]) {
                    getChilds.childNodes[i].classList.toggle("hide-button");
                }
            }    
        }
        e.preventDefault();
    })
}