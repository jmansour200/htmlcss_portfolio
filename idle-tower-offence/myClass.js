/**

Idle Inflation Simulator
Currency: Dollars
Stores: Mint Coins, Mint Notes, Increase Demand (more citizens?), Lower Unemployment, Fear Advertising (inflation expectations), Increase Spending, Foreign Invasion, Corruption
Upgrades: Raise Face Value, Increase Minimum Wage, Quantitative Easing, Debasement, Newspapers, TV, Online, 
Milk: Increase supply? Trade off DPS multiplier for increases Dollar value?

Prestige: Quantitative Tightening/Deflation/Great Depression

constructor (name, price) {
    this.name = name
    this.price = price
}

Game save: Token that is generated based on all of the class values

*/

class Game {
    constructor () {
        this.wallet = "";
        this.stores = [];
        this.upgrades = [];
    }
}

class Save {
    constructor () {
        //Maybe add a fixed DPS and changeable DPS?
        this.save = 0;
    }

    saveGame () {
        // Probably make a JSON export or some kind of algorithmic save
        this.save = 0;
    }

    loadGame (save) {
        this.save = save;
    }

    updateSave (wallet) {
        wallet.updateIncome ();
    }
}

class Wallet {
    constructor () {
        this.income = 0;
        this.balance = 0;
    }

    updateIncome () {
        // Is called and updates this.income whenever certain events happen like buying stores etc
        // Needs a rethink because selling stores doesn't work - need to think how to do it without hardcoding
        // Probably add everything into Save class and run calculations based on that
        //this.income += income;
    }

    updateBalance () {
        // Is called and updates every x period of time (and maybe when mouse click happens)
    }
}

class Store {
    constructor (name, cost, dollars) {
        this.name = name;
        this.cost = cost;
        this.dollars = dollars;
        this.quantity = 0;
        this.income = this.dollars * this.quantity;
    }

    buyStore (save, num) {
        this.cost = this.cost * (1.01 ** num) ;
        this.quantity = this.quantity + num;
        this.income = this.dollars * this.quantity;
        //save.updateSave();
        // Add to Save instead of wallet
    }

    // Don't need this as is, just need to put negative number in buyStore method
    // Could still use it to call buyStore but apply a penalty, e.g. num/5
    // Alternatively, throw an if num is negative statement into buyStore to achieve the same thing
    /*sellStore (num) {
        this.cost = this.cost / (1.01 ** num) ;
        this.quantity = this.quantity - num;
        this.income = this.dollars * this.quantity;
    }*/
}

class Upgrade {
    constructor (name, description, store, cost, multiplier) {
        this.name = name;
        this.description = description;
        this.store = store;
        this.cost = cost;
        this.multiplier = multiplier;
    }

    buyUpgrade (upgrade) {
        upgrade.store.dollars *= 2;
        // Add to Save
    }
}

let game1 = new Game;
let save1 = new Save;
let wallet1 = new Wallet;

let coins = new Store("coins", 1, 0.1);
let notes = new Store("notes", 5, 0.5);
let citizens = new Store("citizens", 25, 2.5);
game1.stores.push(coins, notes, citizens);

let raiseValue = new Upgrade("raiseValue", "Raise Face Value of Coins by 2x", coins, 100, 2);
game1.upgrades.push(raiseValue);

coins.buyStore(wallet1, 10);
raiseValue.buyUpgrade(raiseValue);

// Move this to one of the classes in Update () Method to update balance and income whenever anything changes
// Probably update income on certain methods, balance needs to update every x seconds
//save1.income = save1.income + coins.income + notes.income + citizens.income;
//save1.balance = save1.income * 10;

console.log(game1);