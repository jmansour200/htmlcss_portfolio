<?php

    //header('Content-Type: application/json');

    include('C:/Users/Joel/Documents/htmlcss_portfolio/connections.php');

    function get_youtube($yt_id) {

        global $yt_api_key;

        if (!file_exists(__DIR__ . '/vendor/autoload.php')) {
            throw new Exception(sprintf('Please run "composer require google/apiclient:~2.0" in "%s"', __DIR__));
        }
        require_once __DIR__ . '/vendor/autoload.php';
        
        $client = new Google_Client();
        $client->setApplicationName('YouTube API');
        $client->setDeveloperKey($yt_api_key);
        
        // Define service object for making API requests.
        $service = new Google_Service_YouTube($client);
        
        $queryParams = [
            'id' => $yt_id
        ];
        
        $response = $service->channels->listChannels('snippet,contentDetails,statistics', $queryParams);

        return $response;

    }

    global $yt_id_afl;
    global $yt_id_aflw;
    global $yt_id_keepupfootball;
    global $yt_id_cricketcomau;
    global $yt_id_nrl;
    global $yt_id_nrlw;

    $response_object['afl'] = get_youtube($yt_id_afl);
    $response_object['aflw'] = get_youtube($yt_id_aflw);
    $response_object['keepupfootball'] = get_youtube($yt_id_keepupfootball);
    $response_object['cricketcomau'] = get_youtube($yt_id_cricketcomau);
    $response_object['nrl'] = get_youtube($yt_id_nrl);
    $response_object['nrlw'] = get_youtube($yt_id_nrlw);

    echo json_encode($response_object);
    //echo json_encode(get_youtube($yt_id_afl));
?>